package com.project.interceptor.interceptor.ui

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.widget.Button
import android.widget.EditText
import com.project.interceptor.interceptor.R
import android.widget.Toast
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.annotation.RequiresApi
import com.google.firebase.database.FirebaseDatabase
import com.project.interceptor.interceptor.models.ContactForm
import android.text.TextUtils


class ContactUsActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)


        /* set status bar to black instead od of default background color*/
        window.statusBarColor = Color.BLACK

        val contactButton: Button = findViewById(R.id.contact_button)
        val formName: EditText = findViewById(R.id.contactForm_ime)
        val formEmail: EditText = findViewById(R.id.contactForm_email)
        val formPhone: EditText = findViewById(R.id.contactForm_phone)
        val formMsg: EditText = findViewById(R.id.contactForm_pitanje)



        contactButton.setOnClickListener {

            var name = formName.text
            var email = formEmail.text
            var phone = formPhone.text
            var msg = formMsg.text

            /* Brzinska validacija provjerava samo da nijedno polje nije prazno, ukoliko se taj uvjet ispuni provjerava
            * se još ako je email ispravan i šalje se. Nema točne validacije, ispisuje se toast msg samo */
            if (name.isNotEmpty() && phone.isNotEmpty() && msg.isNotEmpty() && email.isNotEmpty()) {
                if (isValidEmail(email)) {
                    saveMsg(name.toString(), phone.toString(), email.toString(), msg.toString())
                    showToast("Hvala na upitu " + name.toString() + ". Tvoj odgovor stiže ubrzo.")
                    goToHomePage()
                } else {
                    showToast("Provjerite email adresu")
                }
            } else {
                showToast("Sva polja su obavezna")
            }
        }
        val floatingActionButton: FloatingActionButton = findViewById(R.id.menu_back)
        floatingActionButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    private fun goToHomePage() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun sendEmail() {
        val intent = Intent(Intent.ACTION_SENDTO) // it's not ACTION_SEND
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email")
        intent.putExtra(Intent.EXTRA_TEXT, "Body of email")
        intent.data = Uri.parse("mailto:goranx32@gmail.com") // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent)

    }

    private fun saveMsg(name: String, num: String, email: String, msg: String) {
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReferenceFromUrl("https://interceptor-7c4f7.firebaseio.com/")

        var upit: ContactForm = ContactForm(name, num, email, msg)
        myRef.push().setValue(upit)
    }


    fun isValidEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }
}



