package com.project.interceptor.interceptor.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.os.AsyncTask
import java.io.BufferedInputStream
import java.io.InputStream
import java.lang.Exception
import java.net.URL

class Helpers : AsyncTask<String, Void, Bitmap>() {


    override fun doInBackground(vararg params: String?): Bitmap? {
        try {
            val conn = URL(params[0]).openConnection()
            conn.connect()
            val stream: InputStream = conn.getInputStream()
            val bis: BufferedInputStream = BufferedInputStream(stream, 8192)
            val bm: Bitmap   = BitmapFactory.decodeStream(bis)
            return  bm
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }


    fun isConnected(context: Context): Boolean {
        val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
}