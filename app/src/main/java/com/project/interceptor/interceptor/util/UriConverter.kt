package com.project.interceptor.interceptor.util

import android.net.Uri

object UriConverter {

    fun ConvertStringToUri(uriString: String): Uri{
        return Uri.parse(uriString)
    }
}