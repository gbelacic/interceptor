package com.project.interceptor.interceptor.ui

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.widget.LinearLayout
import android.widget.TextView
import com.project.interceptor.interceptor.R
import com.project.interceptor.interceptor.mock.AboutUs

class DefaultMenuItemsTemplateActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_default_menu_items_template)

        /* set status bar to black instead od of default background color*/
        window.statusBarColor = Color.BLACK

        /* set text from mock data */
        val about_us: TextView = findViewById(R.id.about_us_text)
        about_us.text = AboutUs.ABOUT_US

        val contact_msg: LinearLayout = findViewById(R.id.contact_msg)
        contact_msg.setOnClickListener {
            goToContactPage()
        }

        val floatingActionButton: FloatingActionButton = findViewById(R.id.menu_back)
        floatingActionButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun goToContactPage() {
        val intent = Intent(this, ContactUsActivity::class.java)
        startActivity(intent)
    }
}
