package com.project.interceptor.interceptor.helpers

import android.net.Uri

class DataHelper {

    fun convertUri(uriString: String): Uri {
        return Uri.parse(uriString)
    }

}