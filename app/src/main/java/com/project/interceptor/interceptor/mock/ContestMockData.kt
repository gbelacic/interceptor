package com.project.interceptor.interceptor.mock

import android.net.Uri
import com.project.interceptor.interceptor.models.Contest
import com.project.interceptor.interceptor.models.ContestGroup
import com.project.interceptor.interceptor.helpers.DataHelper
import java.util.ArrayList

object ContestMockData {

    /**
     * An array of contests.
     */
    val CONTESTS: MutableList<ContestGroup> = ArrayList()
    val ALL_CONTESTS: MutableList<Contest> = ArrayList()

    init {
        var maloSrednjePoduzetništvo = createContestGoup(0,
                "Malo i srednje poduzetništvo",
                "https://www.interceptorproject.hr/storage/app/media/blog/pokazatelji_javnog_poziva.jpg?fbclid=IwAR1lQp2u14GlG22I5-RWN92w8ve0qvoN8S_EQbDp4aFfU0KhVTGrZkmGIl8")
        var ruralniRazvoj = createContestGoup(1,
                "Ruralni razvoj",
                "https://www.interceptorproject.hr/storage/app/media/ostalo/Foto_ESIF-Zajama-Poljoprivreda-Interceptor-Project.jpg?fbclid=IwAR0NgqsR5CvdAh2vlKrDwkD6Ch2ltY5h_cBMggHL2XPtYl2sw_b4PT9ptF0")
        var civilnoDruštvo = createContestGoup(2,
                "Civilno društvo",
                "https://www.interceptorproject.hr/storage/app/media/ostalo/civilno-drustvo-interceptor-project-foto.jpg?fbclid=IwAR39XdEdHtnnI1bqzO7zPaHbK9yBtQ6OJwbecz2gi4MLrO9TNE1Y2Z0cZDY")
        var javniSektor = createContestGoup(3,
                "Javni sektor",
                "https://www.interceptorproject.hr/storage/app/media/ostalo/Foto_Building_Project.jpg?fbclid=IwAR2aBoCFv-Xr5-02zzjzt22JncTQciKgj_2i-ZyJNu_CMyLQIPKn5F2GRRk")

        var msp3 = createContest(0,
                "Internacionalizacija poslovanja MSP-ova- Faza 2; KK.03.2.1.16.",
                "Bespovratna sredstva",
                "Poduzetništvo",
                "Otvoren",
                "https://efondovi.mrrfeu.hr/MISCms/Pozivi/Poziv?id=46430481-608d-4d81-a2c9-41ceae8aec84",
                "30.10.2018.",
                "58.000.000,00 HRK",
                "Povećati sposobnost hrvatskog gospodarstva za sudjelovanje na globalnim tržištima te pridonijeti povećanju udjela MSP-ova u ukupnom izvozu roba i usluga poboljšanjem uvjeta za njihov rad u međunarodnom okruženju.",
                "Jačanje međunarodne konkurentnosti MSP-ova olakšavanjem predstavljanja njihovih proizvoda/usluga međunarodnoj poslovnoj zajednici u inozemstvu i uvođenja proizvoda na novo, inozemno tržište te povećanja mogućnosti poslovne suradnje s inozemnim partnerima.",
                "100.000,00 HRK.",
                "1.000.000,00 HRK.",
                "● Mikro i malo poduzeće – do 85,00% prihvatljivih troškova\n" +
                        "● Srednje poduzeće – do 65,00% prihvatljivih troškova",
                "● mikro poduzeća\n" +
                        "● mala poduzeća\n" +
                        "● srednja poduzeća",
                "Unutar ovog poziva prihvatljivi troškovi se dijele na one materijalne imovine i nematerijalne. Pod materijalnu imovinu spadaju ove aktivnosti:\n" +
                        "\n" +
                        "● troškovi pripreme (snimak stanja, definicija projekta, modela sustava i postavljanje ciljeva);\n" +
                        "● troškovi pripreme dokumentacije za stjecanje certifikata;\n" +
                        "● uvođenje i certificiranje sustava upravljanja poslovnim procesima i kvalitetom;\n" +
                        "● savjetodavne i konzultantske usluge;\n" +
                        "● nabava (kupnja) norma;\n" +
                        "● edukacija djelatnika;\n" +
                        "● vidljivost projekta.\n" +
                        "Dok je nematerijalna imovina prihvatljiva za izračun troškova ulaganja ako ispunjava sljedeće uvjete:\n" +
                        "\n" +
                        "● mora se upotrebljavati isključivo kod korisnika potpore;\n" +
                        "● mora se voditi kao imovina koja se amortizira;\n" +
                        "● mora biti kupljena po tržišnim uvjetima od treće osobe;\n" +
                        "● mora biti uključena u imovinu poduzetnika koji prima potporu i ostati povezana s projektom tijekom tri godine po završetku projekta.",
                "Nastup na međunarodnom sajmu ili b2b susretu izvan RH koji ima najmanje 10% stranih izlagača/sudionika u ukupnom broju izlagača/sudionika ili 5% inozemnih poslovnih posjetitelja",
                "29.06.2020. godine ili do iskorištenja financijskih sredstava"
        )

        var msp1: Contest = createContest(1,
                "POBOLJŠANJE KONKURENTNOSTI I UČINKOVITOSTI MSP-a KROZ INFORMACIJSKE I KOMUNIKACIJSKE TEHNOLOGIJE (IKT); KK.03.2.1.18",
                "Bespovratna sredstva",
                "Poduzetništvo",
                "U najavi",
                "https://efondovi.mrrfeu.hr/MISCms/Pozivi/Poziv?id=c23897a1-001e-4e9c-9a52-b9bf01f625af",
                "15.11.2018.",
                "200.000.000,00 HRK",
                "Ovim Pozivom potiču se MSP-ovi na primjenu informacijske i komunikacijske tehnologije radi optimiziranja poslovnih procesa, integriranja poslovnih funkcija, učinkovite organizacije tijeka rada, poboljšanja interakcije s klijentima i dobavljačima te poboljšanja tržišnog položaja poduzeća. Svrha Poziva jest jačanje tržišne pozicije, povećanje konkurentnosti i učinkovitosti poslovanja MSP-a primjenom informacijske i komunikacijske tehnologije kao i podrška razvoju informacijskog društva u RH.",
                "Primjena informacijske i komunikacijske tehnologije",
                "80.000,00 HRK",
                "1.000.000,00 HRK",
                "● najviše 80% prihvatljivih troškova će se financirati prihvatljivim prijaviteljima ukoliko se sjedište prijavitelja nalazi u I. i II. skupini JLS\n" +
                        "● najviše 75% prihvatljivih troškova će se financirati prihvatljivim prijaviteljima ukoliko se sjedište prijavitelja nalazi u III. i IV. skupini JLS\n" +
                        "● najviše 70% prihvatljivih troškova financirati će se prihvatljivim prijaviteljima ukoliko se sjedište prijavitelja nalazi u V. i VI. skupini JLS\n" +
                        "● najviše 65% prihvatljivih troškova financirati će se prihvatljivim prijaviteljima ukoliko se sjedište prijavitelja nalazi u VII. I VIII. skupini JLS.",
                "● Mikro poduzetnici\n" +
                        "● Mali poduzetnici\n" +
                        "● Srednji poduzetnici",
                "● troškovi nabave standardnih i out-of-box softvera i izrade/razvoja specifičnog softvera (dizajniran po mjeri za potrebe korisnika), uključujući i njegovo instaliranje, programiranje, testiranje i/ili konfiguraciju za potrebe korisnika (npr. DMS, ERP, CRM, HRM, CMS, ECM, CAD, CAM…)\n" +
                        "● troškovi za licence za nadogradnju softvera (trajne ili najam do godine dana)\n" +
                        "● troškovi za korištenje SaaS (Software as a Service) modela/usluge (nabava prava korištenja u razdoblju od najviše godine dana od dana nabave usluge)\n" +
                        "● troškovi nabave računalne i komunikacijske opreme (hardware), uključujući i instaliranje, programiranje i/ili konfiguraciju isključivo za potrebe provedbe aktivnosti\n" +
                        "● izdaci za ostalu opremu i uređaje potrebne isključivo za izravne projektne aktivnosti (UPS, aktivna i pasivna mrežna oprema, mrežni ormar/rack, itd.)\n" +
                        "● edukacija zaposlenika za korištenje novih implementiranih sustava kao dio isporuke projekta\n" +
                        "● troškovi usluga vanjskih stručnjaka za pripremu projektnog prijedloga (prihvatljivo od 9. srpnja 2018. godine) do iznosa 15.000,00 HRK\n" +
                        "● troškovi usluga vanjskih stručnjaka za pripremu i provođenje postupaka nabave, vođenje projekta do iznosa 15.000,00 HRK\n" +
                        "● troškovi vezani za ispunjavanje uvjeta informiranja i vidljivosti.",
                "",
                "60 kalendarskih dana od datuma objave Javnog Poziva")
        var msp2: Contest = createContest(2,
                "Izgradnja i opremanje proizvodnih kapaciteta MSP KK.03.2.1.10",
                "Bespovratna sredstva",
                "Poduzetništvo",
                "Zatvoren",
                "https://strukturnifondovi.hr/natjecaji/izgradnja-i-opremanje-proizvodnih-kapaciteta-msp/",
                "30.4.2018",
                "200.000.000,00 HRK.",
                "Jačanje regionalne konkurentnosti MSP-a kroz razvoj identificiranih strateških djelatnosti industrije. Poticanjem aktivnosti koje stvaraju dodatnu vrijednost i učinkovitim pozicioniranjem malog i srednjeg poduzetništva u globalnim lancima vrijednosti omogućit će se razvoj novih kompetencija, povećanje proizvodnje, povećanje izvoza i stvaranje novih radnih mjesta.",
                "Jačanje regionalne konkurentnosti MSP-a",
                "500.000,00 HRK",
                "15.000.000,00 HRK",
                "● Regionalne potpore:\n" +
                        "\t● 35% srednji poduzetnici;\n" +
                        "\t● 45% mikro i mali poduzetnici;\n" +
                        "● Potpore za savjetodavne usluge u korist MSP-ova: 50%\n" +
                        "● Potpore MSP-ovima za sudjelovanje na sajmovima: 50%\n" +
                        "● Potpore za usavršavanje:\n" +
                        "\t● 60% srednji poduzetnici (70% ako se usavršavanje provodi za radnike s invaliditetom ili radnike u nepovoljnom položaju);\n" +
                        "\t● 70% mikro i mali poduzetnici;\n" +
                        "● Potpore male vrijednosti: 50%",
                "mikro, malo ili srednje poduzeće.\n" +
                        "\n" +
                        "Prihvatljivi prijavitelji moraju do trenutka prijave projekta biti registrirani u sektorima u kojima se trebaju odvijati aktivnosti projekta, a koji obuhvaćaju sljedeće sektore Nacionalne klasifikacije djelatnosti:\n" +
                        "\n" +
                        "● C Prerađivačka industrija;\n" +
                        "● J Informacije i komunikacije;",
                "● ulaganje u materijalnu i nematerijalnu imovinu;\n" +
                        "● savjetodavne usluge koje pružaju vanjski konzultanti;\n" +
                        "● sudjelovanje na sajmovima;\n" +
                        "● usavršavanje;\n" +
                        "● priprema Prijavnog obrasca, Investicijske studije s proračunom projekta;\n" +
                        "● uvođenje grijanja i hlađenja;\n" +
                        "● promidžba i vidljivost;",
                "",
                "28.12.2018.")

        var msp4: Contest = createContest(4,
                "Uvođenje sustava upravljanja poslovnim procesima i kvalitetom (ISO i slične norme); KK.03.2.1.14.",
                "Bespovratna sredstva",
                "Poduzetništvo",
                "Otvoren",
                "https://efondovi.mrrfeu.hr/MISCms/Pozivi/Poziv?id=635a567d-1ea5-424c-9ede-8a44453bf898",
                "14.05.2018.",
                "38.000.000,00 HRK",
                "Dostizanje primjenjive razine standarda međunarodno priznatih razina kvalitete i sigurnosti u razmjeni roba i usluga kroz povećanje uporabe priznatih normi koje pridonose povjerenju kupaca, posebno u područjima poboljšanja kvalitete sustava upravljanja i povećanja sposobnosti za dokazivanje kvalitete sustava, čime bi se MSP-ovima olakšao pristup inozemnim tržištima i povećala konkurentnost.",
                "Poduprijeti mikro, male i srednje poduzetnike da uvođenjem priznatih sustava upravljanja poslovnim procesima i kvalitetom, primjenom norma te certifikacijom, u skladu sa zahtjevima međunarodno prihvaćenih norma ili tržišno priznatih certifikacijskih shema, povećaju svoju konkurentnost na tržištu.",
                "50.000,00 HRK",
                "380.000,00 HRK",
                "● Mikro i malo poduzeće – do 85,00% prihvatljivih troškova\n" +
                        "● Srednje poduzeće – do 65,00% prihvatljivih troškova",
                "● mikro poduzeća;\n" +
                        "● mala poduzeća;\n" +
                        "● srednja poduzeća.\n" +
                        "U ovom pozivu prijavitelj može poslati samo jedan projektni prijedlog, te partnerstvo nije dopušteno. Po pozitivnoj odluci o dodjeli bespovratnih sredstava, korisnik može zatražiti predujam do najviše 40% odobrenih sredstava.",
                "Unutar ovog poziva prihvatljivi troškovi se dijele na one materijalne imovine i nematerijalne. Pod materijalnu imovinu spadaju ove aktivnosti:\n" +
                        "\n" +
                        "● troškovi pripreme (snimak stanja, definicija projekta, modela sustava i postavljanje ciljeva);\n" +
                        "● troškovi pripreme dokumentacije za stjecanje certifikata;\n" +
                        "● uvođenje i certificiranje sustava upravljanja poslovnim procesima i kvalitetom;\n" +
                        "● savjetodavne i konzultantske usluge;\n" +
                        "● nabava (kupnja) norma;\n" +
                        "● edukacija djelatnika;\n" +
                        "● vidljivost projekta.\n" +
                        "\n" +
                        "Dok je nematerijalna imovina prihvatljiva za izračun troškova ulaganja ako ispunjava sljedeće uvjete:\n" +
                        "\n" +
                        "● mora se upotrebljavati isključivo kod korisnika potpore;\n" +
                        "● mora se voditi kao imovina koja se amortizira;\n" +
                        "● mora biti kupljena po tržišnim uvjetima od treće osobe;\n" +
                        "● mora biti uključena u imovinu poduzetnika koji prima potporu i ostati povezana s projektom tijekom tri godine po završetku projekta.",
                "Kroz ovaj Poziv će poduzetnici moći dobiti različite certifikate poput certifikata ISO Sustav upravljanja kvalitetom ili Programa za poticanje certificiranja šuma (PEFC) te mnoge druge.\n" +
                        "\n" +
                        "Aktivnosti certifikacije / certificiranja sustava upravljanja mogu biti prihvatljive samo ako su provedene od strane tijela akreditiranog za uvođenje sustava upravljanja poslovnim procesima i kvalitetom. U slučaju da za neku normu ne postoji akreditirano tijelo, prihvatljivim pružateljem smatra se i neakreditirano tijelo, pod uvjetom da je registrirano za obavljanje te djelatnosti.\n" +
                        "\n" +
                        "Održivost projekta će se gledati 3 godine nakon njegovog završetka. Pokazatelj neposrednih rezultata projekta će biti broj sustava upravljanja kvalitetom ili poslovnim procesima uveden u poslovanje (certificiran).",
                "29.06.2020.g. (projekti iz ovog poziva moraju završiti do 31. prosinca 2021. godine.)")
        var msp5: Contest = createContest(5,
                "Certifikacijom proizvoda do tržišta KK.03.2.1.08",
                "Bespovratna sredstva",
                "Poduzetništvo",
                "U najavi",
                "https://strukturnifondovi.hr/natjecaji/certifikacijom-proizvoda-do-trzista/",
                "U najavi",
                "30.000.000,00 HRK",
                "povećanom primjenom normi, zajedno s pouzdanim tehnološkim rješenjem, pridonijeti aktivnostima MSP-ova da dokazom kvalitete, sigurnosti i pouzdanosti svojih proizvoda osiguraju preduvjete za povećanje izvoza i ukupne konkurentnosti. Pružanjem potpore za uvođenje i primjenu europskih i međunarodno (globalno) priznatih norma doprinosi se da MSP-ovi iskoriste prednosti koje primjena normi ima na omogućavanje pristupa novim tržištima.",
                "● certifikacija proizvoda, odnosno ocjenjivanje sukladnosti proizvoda s određenom normom ili specifikacijom (u skladu s direktivama EU i drugih zemalja i odgovarajućim normama na koje se pozivaju direktive);\n" +
                        "● sve radnje u okviru postupaka ocjenjivanja sukladnosti;\n" +
                        "● aktivnosti certifikacije mogu biti prihvatljive samo ukoliko je neovisno ocjenjivanje sukladnosti provedeno od strane ovlaštenog tijela;\n" +
                        "Prihvatljive kategorije troškova mogu biti sve radnje u okviru postupka ocjenjivanja sukladnosti što uključuje:",
                " 20.000,00 HRK",
                "1.000.000,00 HRK",
                "● mikro i mala poduzeća (do 85% ukupnih prihvatljivih troškova)\n" +
                        "● srednja poduzeća (do 65% ukupnih prihvatljivih troškova)",
                "mikro, malo ili srednje poduzeće.",
                "● troškovi pripreme propisane tehničke dokumentacije (tehnička datoteka koja prati spis) ukoliko istu priprema vanjski pružatelj usluge izrade tehničke dokumentacije\n" +
                        "● prijevod tehničke dokumentacije na strani jezik, ukoliko je neophodno za ocjenjivanje sukladnosti\n" +
                        "● troškovi transporta proizvoda do akreditiranog tijela za ocjenjivanje sukladnosti\n" +
                        "● ocjenjivanje sukladnosti proizvoda (ispitivanje, pregled, certifikacija, mjerenje, umjeravanje) od strane akreditiranog tijela za ocjenu sukladnosti\n" +
                        "● troškovi postupka ocjene sukladnosti proizvoda i izdavanje isprava o sukladnosti (na primjer: izvještaj o ispitivanju, certifikat, potvrda o sukladnosti, potvrda o pregledu, izjava o svojstvima) od strane akreditiranog tijela za ocjenu sukladnosti\n" +
                        "● troškovi u svezi s ispunjavanjem uvjeta informiranja",
                "",
                "29.06.2020.")
        var msp6: Contest = createContest(6,
                "ZNAKOVI KVALITETE",
                "Bespovratna sredstva",
                "Poduzetništvo",
                "Otvoren",
                "https://efondovi.mrrfeu.hr/MISCms/Pozivi/Poziv?id=1e4bafa0-d138-40f5-82bb-b16d488cab63",
                "13.07.2018.",
                "7.500.000,00 HRK",
                "Primjena znakova kvalitete doprinijet će povećanju prepoznatljivosti kvalitete usluga i proizvoda MSP-ova. Time će se osigurati preduvjeti za povećanje prihoda od prodaje, izvoza i ukupne konkurentnosti i posljedično, doprinijeti stvaranju hrvatskog identiteta na zajedničkom i svjetskom tržištu.",
                "Doprinos prepoznatljivosti kvalitete hrvatskih proizvoda i usluga kroz pružanje potpore MSP-ovima za ishođenje znaka kvalitete njihovih proizvoda, usluga ili djelatnosti koji posjeduju natprosječnu kvalitetu, a proizvedeni su na području Republike Hrvatske ili su nastali kao rezultat hrvatske tradicije, razvojno-istraživačkog rada, inovacije i invencije.",
                "4.000,00 HRK",
                "75.000,00 HRK",
                "● intenzitet potpore koji se može dodijeliti iznosi do 100,00% prihvatljivih troškova po pojedinom projektom prijedlogu.",
                "Prijavitelj mora biti pravna ili fizička osoba koja je mikro, mali ili srednji poduzetnik sukladno definiciji malih i srednjih poduzeća na način utvrđen u Prilogu I. \"Definicija MSP-ova\" Uredbe 651/2014.",
                "● troškovi postupka za izdavanje/dodjelu prava korištenja znaka kvalitete i troškovi naknade za pravo korištenja znaka kvalitete u prvom razdoblju korištenja.\n" +
                        "Dodijeljenim bespovratnim sredstvima, putem vaučera sufinancirat će se projektne aktivnosti koje se odnose na ostvarivanje prava korištenja sljedećih znakova kvalitete:\n" +
                        " \n" +
                        "● „Hrvatska kvaliteta“,\n" +
                        "● „Izvorno hrvatsko“,\n" +
                        "● „Tradicijski obrti”,\n" +
                        "● „Umjetnički obrti”.\n" +
                        " Vaučer se može dodijeliti za nabavu znaka/znakova od prihvatljivog pružatelja usluge:\n" +
                        " \n" +
                        "● Hrvatske gospodarske komore,\n" +
                        "● Hrvatske obrtničke komore.",
                "",
                "29.06.2020.")
        var msp7: Contest = createContest(7,
                "Inovacijski vaučeri za MSP-ove; KK.03.2.2.03",
                "Bespovratna sredstva",
                "Poduzetništvo",
                "Otvoren",
                "https://efondovi.mrrfeu.hr/MISCms/Pozivi/Poziv?id=cfc4e79b-68c5-4ce8-a481-04ee5d76dda2",
                "21. lipnja 2018.",
                "50.000.000,00 HRK",
                "Pružanje stručne podrške od strane ZIO u vidu ugovornog pružanja usluga MSP-ovima za troškove testiranja, ispitivanja, demonstracijskih aktivnosti, kao i korištenja stručnih tehničkih znanja za potrebe inovativnih procesa i komercijalizacije inovacija.",
                "Jačanje kapaciteta MSP-ova za istraživanje, razvoj i inovacije kroz poticanje suradnje sa znanstveno-istraživačkim organizacijama (u daljnjem tekstu: ZIO) u svrhu razvoja novih proizvoda, usluga ili procesa, s naglaskom na komercijalizaciju proizvoda i usluga.",
                "10.000,00 HRK",
                "75.000,00 HRK",
                "Za dodjelu bespovratnih sredstava prijaviteljima koji su registrirani na područjima jedinica područne (regionalne) samouprave (JP(R)S) svrstanima na temelju Odluke o razvrstavanju jedinica lokalne i područne (regionalne) samouprave prema stupnju razvijenosti (NN 132/17) utvrđuju se slijedeći intenziteti potpora:\n" +
                        " \n" +
                        "● 85% prihvatljivih troškova - sredstva namijenjena za I. skupinu JP(R)S;\n" +
                        "● 80% prihvatljivih troškova - sredstva namijenjena za II. skupinu JP(R)S;\n" +
                        "● 75% prihvatljivih troškova -sredstva namijenjena za III. skupinu JP(R)S;\n" +
                        "● 70% prihvatljivih troškova - sredstva namijenjena za IV. skupinu JP(R)S.",
                "Prijavitelj mora biti pravna ili fizička osoba koja je mikro, mali ili srednji poduzetnik sukladno definiciji malih i srednjih poduzeća na način utvrđen u Prilogu I. Definicija malih i srednjih poduzeća Uredbe 651/2014.",
                "Prihvatljive aktivnosti koje se mogu financirati u okviru ovog Poziva su aktivnosti namijenjene inovacijama koje trebaju rezultirati proizvodima, uslugama ili procesima koje su novost u ponudi poduzeća i/ili novost na tržištu:\n" +
                        "\n" +
                        "● Testiranje proizvoda, usluge ili procesa;\n" +
                        "● Ispitivanje proizvoda, usluge ili procesa;\n" +
                        "● Demonstracijske aktivnosti;\n" +
                        "● Stručna i tehnička znanja u svrhu razvoja proizvoda, usluga i procesa.",
                "",
                "Poziv se vodi kao trajni otvoreni Poziv na dostavu projektnih prijava s krajnjim rokom dostave projektnih prijava do 29. lipnja 2020. godine u 16:00 sati.")
        var msp8: Contest = createContest(8,
                "ESIF – Mali zajam",
                "Kreditna sredstva",
                "Poduzetništvo",
                "Otvoren",
                "http://hamagbicro.hr/wp-content/uploads/2017/10/Program-ESIF-mali-zajmovi.pdf",
                "17.09.2018.",
                "50.000.000,00 €",
                "Cilj Financijskog instrumenta je kreditiranje mikro, malih i srednjih subjekata malog\n" +
                        "gospodarstva uz povoljnije uvjete financiranja. Uz smanjenje kamatne stope i smanjenje\n" +
                        "razine potrebnih sredstava osiguranja cilj je povećati dostupnost zajmova ovoj skupini\n" +
                        "poduzetnika.\n",
                "kreditiranje mikro, malih i srednjih subjekata malog" +
                        "gospodarstva uz povoljnije uvjete financiranja. ",
                "25.000,00 €",
                "50.000,00 €",
                "● zadužnice korisnika zajma\n" +
                        "● hipoteka na nekretninu\n" +
                        "● ostali instrumenti osiguranja ovisno o procjeni rizika",
                "● Mikro gospodarski subjekt;\n" +
                        "● Mali gospodarski subjekti;\n" +
                        "● Fizičke osobe- u trenutku podnošenja zahtjeva za kredit nemaju registrirani vlastiti gospodarski subjekt - ukoliko se zajam odobri potrebno je registrirati gospodarski subjekt jer se Ugovor o zajmu ugovara isključivo sa registriranim gospodarskim subjektom;",
                "1. Materijalna imovina\n" +
                        " \n" +
                        "● Osnivačka ulaganja;\n" +
                        "● Zemljišta, građevinski objekti;\n" +
                        "● Oprema i uređaji;\n" +
                        "\n" +
                        "2. Nematerijalna ulaganja\n" +
                        " \n" +
                        "● Razvoj proizvoda ili usluga, patenti, licence, koncesije, autorska prava, franšize;\n" +
                        "\n" +
                        "3. Obrtna sredstva\n" +
                        " \n" +
                        "● Trajna obrtna sredstva (do 30% iznosa zajma);",
                "",
                "31.12.2020.g ili do iskorištenja sredstava.")
        var msp9: Contest = createContest(9,
                "ESIF – Mikro zajam",
                "Kreditna sredstva",
                "Poduzetništvo",
                "Otvoren",
                "http://hamagbicro.hr/wp-content/uploads/2017/10/Program-ESIF-mikro-investicijski-zajmovi.pdf",
                "17.09.2018.",
                "50.000.000,00 €",
                "Cilj Financijskog instrumenta je kreditiranje mikro i malih subjekata malog gospodarstva uz\n" +
                        "povoljnije uvjete financiranja",
                "Cilj Financijskog instrumenta je kreditiranje mikro i malih subjekata malog gospodarstva uz\n" +
                        "povoljnije uvjete financiranja. Uz smanjenje kamatne stope i smanjenje razine potrebnih\n" +
                        "sredstava osiguranja cilj je povećati dostupnost zajmova ovoj skupini poduzetnika.",
                "1.000 €",
                "25.000 €",
                "",
                "● Mikro gospodarski subjekt;\n" +
                        "● Mali gospodarski subjekti;\n" +
                        "● Fizičke osobe- u trenutku podnošenja zahtjeva za kredit nemaju registrirani vlastiti gospodarski subjekt - ukoliko se zajam odobri potrebno je registrirati gospodarski subjekt jer se Ugovor o zajmu ugovara isključivo sa registriranim gospodarskim subjektom;",
                "1. Materijalna imovina\n" +
                        "\n" +
                        "● Osnivačka ulaganja;\n" +
                        "● Zemljište, građevinski objekti;\n" +
                        "● Oprema i uređaji;\n" +
                        "\n" +
                        "2. Nematerijalna imovina\n" +
                        " \n" +
                        "● Razvoj proizvoda ili usluga, patenti, licence, koncesije, autorska prava, franšize;\n" +
                        "\n" +
                        "3. Obrtna sredstva\n" +
                        " \n" +
                        "● trajna obrtna sredstva (do 30% iznosa zajma);",
                "",
                "31.12.2020.g ili do iskorištenja sredstava.")
        var msp10: Contest = createContest(10,
                "HBOR StartUp Zajam",
                "Kreditna sredstva",
                "Poduzetništvo",
                "Otvoren",
                "https://www.hbor.hr/kreditni_program/pocetnici-start-up/",
                "15.10.2018.",
                "15.200.000,00 HRK",
                "kreditiranje novih trgovačkih društava, obrta te otvaranje novih radnih mjesta. Korisnici mogu biti poduzetnici koji po prvi put osnivaju obrt ili trgovačko društvo i koji će nakon realizacije ulaganja u osnovanom trgovačkom društvu ili obrtu biti aktivno i stalno zaposleni. Početnicima se smatraju zadruge kao i mali i srednji poduzetnici koji posluju kraće od dvije godine.",
                "Kreditiranje novih trgovačkih društava",
                "80.000,00 HRK",
                "1.800.000,00 HRK",
                "● najviše 85% prihvatljivih troškova ukoliko se sjedište prijavitelja nalazi u I. skupini JP(R)S ;\n" +
                        "● najviše 80% prihvatljivih troškova ukoliko se sjedište prijavitelja nalazi u II. skupini JP(R)S;\n" +
                        "● najviše 75% prihvatljivih troškova ukoliko se sjedište prijavitelja nalazi u III. skupini JP(R)S;\n" +
                        "● najviše 70% prihvatljivih troškova ukoliko se sjedište prijavitelja nalazi u IV. skupini JP(R)S;",
                "Poduzetnici koji:\n" +
                        " \n" +
                        "● po prvi put osnivaju obrt, trgovačko društvo ili obiteljsko poljoprivredno gospodarstvo,\n" +
                        "● nakon realizacije ulaganja, u osnovanom trgovačkom društvu ili obrtu će biti aktivno i stalno zaposleni na odgovornom radnom mjestu,\n" +
                        "● su mlađi od 55 godina,\n" +
                        "● nisu vlasnici drugog trgovačkog društva, obrta ili obiteljskog poljoprivrednog gospodarstva,\n" +
                        "● nisu suvlasnici u drugom trgovačkom društvu, obrtu ili obiteljskom poljoprivrednom gospodarstvu s udjelom većim od 30%\n" +
                        " Početnicima se smatraju i zadruge i mali i srednji poduzetnici koji posluju kraće od dvije godine.",
                "Prihvatljive aktivnosti koje se mogu financirati u okviru ovog Poziva su aktivnosti namijenjene inovacijama koje trebaju rezultirati proizvodima, uslugama ili procesima koje su novost u ponudi poduzeća i/ili novost na tržištu:\n" +
                        "\n" +
                        "● Testiranje proizvoda, usluge ili procesa;\n" +
                        "● Ispitivanje proizvoda, usluge ili procesa;\n" +
                        "● Demonstracijske aktivnosti;\n" +
                        "● Stručna i tehnička znanja u svrhu razvoja proizvoda, usluga i procesa.",
                "",
                "29.06.2020 godine.")


        var ruralni1: Contest = createContest(11,
                "Tip operacije 6.3.1. “Potpora razvoju malih poljoprivrednih gospodarstava”",
                "Bespovratna sredstva",
                "Poljoprivreda",
                "Otvoren",
                "https://ruralnirazvoj.hr/natjecaj-za-tip-operacije-6-3-1-potpora-razvoju-malih-poljoprivrednih-gospodarstava/",
                "20.10.2018.",
                "279.000.000,00 HRK",
                "pomoći malim poljoprivrednim gospodarstvima u njihovu prijelazu na tržišno orijentiranu proizvodnju, održivom razvoju, uključenje članova gospodarstva u rad na poljoprivrednom gospodarstvu i zaradu dovoljno prihoda za dostojanstven život za ostanak na ruralnom području čime se pridonosi smanjenju nezaposlenosti na ruralnom području.",
                "pomoć malim poljoprivrednim gospodarstvima u njihovu prijelazu na tržišno orijentiranu proizvodnju",
                " od 5.000 EUR-a po korisniku.",
                " do 15.000 EUR-a po korisniku.",
                " 100% ",
                "● Obiteljska poljoprivredna gospodarstva;\n" +
                        "● Obrt registriran za poljoprivrednu djelatnost;\n" +
                        "● Trgovačko društvo za poljoprivrednu djelatnost;\n" +
                        "● Zadruga za poljoprivrednu djelatnost.\n" +
                        "● * Prihvatljivi korisnici su mala poljoprivredna gospodarstava upisana u Upisnik poljoprivrednika, ekonomske veličine iskazane u ukupnom standardnom ekonomskom rezultatu poljoprivrednog gospodarstva od 2.000 eura do 7.999 eura.",
                "● kupnja domaćih životinja, jednogodišnjeg i višegodišnjeg bilja, sjemena i sadnog materijala;\n" +
                        "● kupnja, građenje i/ili opremanje zatvorenih/zaštićenih prostora i objekata te ostalih gospodarskih objekata uključujući vanjsku i unutarnju infrastrukturu u sklopu poljoprivrednog gospodarstva u svrhu obavljanja poljoprivredne proizvodnje i/ili prerade proizvoda;\n" +
                        "● kupnja ili zakup poljoprivrednog zemljišta;\n" +
                        "● kupnja poljoprivredne mehanizacije, strojeva i opreme;\n" +
                        "● podizanje novih i/ili restrukturiranje postojećih višegodišnjih nasada;\n" +
                        "● uređenje i poboljšanje kvalitete poljoprivrednog zemljišta u svrhu poljoprivredne proizvodnje;\n" +
                        "● građenje i/ili opremanje objekata za prodaju i prezentaciju vlastitih poljoprivrednih proizvoda uključujući i troškove promidžbe vlastitih poljoprivrednih proizvoda;\n" +
                        "● stjecanje potrebnih stručnih znanja i sposobnosti za obavljanje poljoprivredne proizvodnje i prerade proizvoda;\n" +
                        "● operativno poslovanje poljoprivrednog gospodarstva.",
                "",
                "Zahtjev za potporu može se popunjavati i podnositi u AGRONET-u od 30. kolovoza 2018. godine od 12:00 sati do 13. prosinca 2018. godine do 12:00 sati.")
        var ruralni2: Contest = createContest(12,
                "Tip operacije, „3.2.1. Potpora za aktivnosti informiranja i promoviranja“",
                "Bespovratna sredstva.",
                "Poljoprivreda",
                "Otvoren",
                "https://www.apprrr.hr/podmjera-3-2-potpora-za-aktivnosti-informiranja-i-promicanja-koje-provode-skupine-proizvodaca-na-unutarnjem-trzistu/",
                "15.11.2018.",
                "10.700.000,00 HRK.",
                "Sufinanciranje troškova za provođenje aktivnosti predviđenih Planom informiranja i promoviranja, a u cilju poboljšanja svijesti potrošača o postojanju i specifikaciji proizvoda proizvedenih u okviru sustava zaštićenih oznaka izvornosti (ZOI) i zaštićenih oznaka zemljopisnog podrijetla (ZOZP) te sustava zajamčeno tradicionalnog specijaliteta (ZTS), kao i u sustavu ekološke proizvodnje.",
                "Sufinanciranje troškova za provođenje aktivnosti predviđenih Planom informiranja i promoviranja, a u cilju poboljšanja svijesti potrošača",
                "0,00 HRK",
                "743.000,00 HRK",
                "100%",
                "Skupine proizvođača, bez obzira na pravni oblik, sastavljene uglavnom od proizvođača ili prerađivača istog proizvoda, a koji sudjeluju u sustavima kvalitete (ZOI – Zaštićena oznaka izvornosti, ZOZP – Zaštićena oznaka zemljopisnog podrijetla, ZTS – Zajamčeno tradicionalni specijalitet), udruge ekoloških poljoprivrednih proizvođača čiji su članovi proizvođači uključeni u ekološku proizvodnju u skladu s nacionalnim zakonodavstvom.",
                "● organizacija sajmova, izložbi, manifestacija i drugih namjenskih promotivnih događanja gdje će se promovirati proizvodi iz sustava kvalitete i ekološki poljoprivredni proizvodi\n" +
                        "● sudjelovanja korisnika na sajmovima, izložbama, manifestacijama i drugim namjenskim promotivnim događanjima gdje će se promovirati proizvodi iz sustava kvalitete i ekološki poljoprivredni proizvodi\n" +
                        "● organizacija radionica, seminara i konferencija\n" +
                        "● sudjelovanja korisnika na radionicama, seminarima i konferencijama\n" +
                        "● izrada promotivnih materijala\n" +
                        "● izrada, razvoj i održavanje interaktivne mrežne stranice skupine/udruge namijenjene promoviranju proizvoda iz sustava kvalitete i ekoloških proizvoda\n" +
                        "● zakup oglasnog prostora\n" +
                        "● informativne i promotivne aktivnosti putem različitih kanala komunikacije, aktivnosti na prodajnim mjestima od nacionalnog ili EU značaja ili kroz HoReCa",
                "",
                "31.01.2019. godine ili do iskorištenja sredstava.")
        var ruralni3: Contest = createContest(13,
                "Tip operacije 8.6.1 „Modernizacija tehnologija, strojeva, alata i opreme u pridobivanju drva i šumskouzgojnim radovima“",
                "Bespovratna sredstva",
                "Poljoprivreda",
                "Otvoren",
                "https://ruralnirazvoj.hr/tip-operacije-8-6-1-modernizacija-tehnologija-strojeva-alata-i-opreme-u-pridobivanju-drva-i-sumskouzgojnim-radovima/",
                "01.11.2018.",
                "70.000.000,00 HRK",
                "dodjela sredstava projektima koji će omogućiti modernizaciju tehnologija, strojeva, alata i opreme u pridobivanju drva i šumskouzgojnim radovima.",
                "dodjela potpore na temelju Pravilnika o provedbi mjere 8, podmjere 8.6 „Potpora za ulaganja u šumarske tehnologije i preradu, mobilizaciju i marketing šumskih proizvoda“ iz Programa ruralnog razvoja Republike Hrvatske za razdoblje 2014. -2020.",
                "5.000,00 €",
                "700.000,00 €",
                "intenzitet potpore koji se može dodijeliti iznosi najviše 50,00% prihvatljivih troškova po pojedinom projektom prijedlogu.",
                "● šumoposjednici,\n" +
                        "● udruge šumoposjednika,\n" +
                        "● obrti, mikro, mala i srednja poduzeća.",
                "● troškovi usluga arhitekata, inženjera i konzultanata i\n" +
                        "● troškovi izrade studija izvedivosti, elaborata/studija utjecaja zahvata na okoliš/ekološku mrežu i slično.\n" +
                        "Opći troškovi prihvatljivi su do 10% vrijednosti ukupno prihvatljivih troškova projekta, pri čemu su:\n" +
                        "\n" +
                        "● troškovi pripreme poslovnog/idejnog plana prihvatljivi u iznosu do 2% od ukupno prihvatljivih troškova projekta bez općih troškova, ali ne više od 5.000 eura u kunskoj protuvrijednosti,\n" +
                        "● troškovi pripreme i/ili provedbe projekta prihvatljivi u iznosu do 2% od ukupno prihvatljivih troškova projekta bez općih troškova, ali ne više od 10.000 eura u kunskoj protuvrijednosti,\n" +
                        "● troškovi projektno-tehničke dokumentacije, geodetskih podloga, elaborata i trošak nadzora prihvatljivi u iznosu koji čini razliku zbroja troškova navedenih u točkama a) i b) ovoga stavka i gornje granice od 10 % od ukupno prihvatljivih troškova projekta bez općih troškova, ako je primjenjivo.",
                "",
                " 14. prosinac, 2018. godine ili do iskorištenja financijskih sredstava")

        var civilno1: Contest = createContest(14,
                "Unaprjeđenje usluga za djecu u sustavu ranog i predškolskog odgoja i obrazovanja UP.02.2.2.08.",
                "Bespovratna sredstva",
                "Civilno društvo",
                "Otvoren",
                "https://strukturnifondovi.hr/natjecaji/unaprjedenje-usluga-za-djecu-u-sustavu-ranog-predskolskog-odgoja-obrazovanja/",
                "08.03.2018.",
                "300.000.000,00 HRK",
                "doprinijeti usklađivanju poslovnog i obiteljskog života obitelji s uzdržavanim članovima uključenim u programe ranog i predškolskog odgoja i obrazovanja.",
                "Otvoreni poziv na dostavu projektnih prijedloga (bespovratna sredstva)",
                "15.000.000,00 HRK",
                "500.000,00 HRK",
                "100%",
                "● dječji vrtić sukladno popisu Ministarstva znanosti i obrazovanja\n" +
                        "● jedinica lokalne samouprave",
                "● razvoj posebnih programa\n" +
                        "● promidžba i vidljivost\n" +
                        "● upravljanje projektom i administracija.",
                "",
                "31.12.2019.")
        var civilno2: Contest = createContest(15,
                "Podrška socijalnom uključivanju i zapošljavanju marginaliziranih skupina",
                "Bespovratna sredstva",
                "Socijalna uključenost",
                "Obustavljen",
                "https://strukturnifondovi.hr/natjecaji/podrska-socijalnom-ukljucivanju-i-zaposljavanju-marginaliziranih-skupina/",
                "22.09.2017.",
                "99.918.750,00 HRK",
                "● Jačanje stručnih znanja nezaposlenih osoba pripadnika marginaliziranih skupina kroz provedbu programa obrazovanja odraslih;\n" +
                        "● Osnaživanje te razvoj mekih i/ili transverzalnih (prenosivih) vještina nezaposlenih osoba pripadnika marginaliziranih skupina provedbom ciljanih programa, radionica i/ili pružanja usluga mentorstva;\n" +
                        "● Jačanje kapaciteta stručnjaka u svrhu unaprjeđenja usluga povezanih s pristupom tržištu rada i socijalnim uključivanjem za nezaposlene osobe pripadnike marginaliziranih skupina.",
                "Otvoreni poziv na dostavu projektnih prijedloga (bespovratna sredstva)",
                "350.000,00 HRK",
                "1.500.000,00 HRK",
                "Intenzitet potpore po pojedinom Projektu može iznositi do 100% prihvatljivih troškova, odnosno prijavitelji/partneri nisu dužni osigurati vlastito sufinanciranje.",
                "● Udruge,\n" +
                        "● Javne ustanove,\n" +
                        "● Zaklade,\n" +
                        "● Jedinice lokalne ili regionalne samouprave.",
                "● razvoj i provedba ciljanih programa za osnaživanje marginaliziranih skupina\n" +
                        "● jačanje kapaciteta stručnjaka iz različitih sektora koji rade s nezaposlenim osobama pripadnicima marginaliziranih skupina u svrhu unaprjeđenja usluga povezanih sa pristupom tržištu rada i socijalnim uključivanjem pripadnika ciljnih skupina\n" +
                        "● promidžba",
                "",
                "31.12.2019.")
        var civilno3: Contest = createContest(16,
                "Promocija zdravlja i prevencija bolesti – Faza 1",
                "Bespovratna sredstva",
                "Zdravlje",
                "Obustavljen",
                "https://strukturnifondovi.hr/natjecaji/promocija-zdravlja-i-prevencija-bolesti-faza-1/",
                "30.03.2018.",
                "27.000.000,00HRK",
                "Povećanje znanja i svijesti o važnosti promocije zdravlja i prevencije bolesti na području Republike Hrvatske.",
                "Otvoreni poziv na dostavu projektnih prijedloga (bespovratna sredstva)",
                "200.000,00 HRK",
                "1.000.000,00 HRK",
                "Intenzitet potpore po pojedinom Projektu može iznositi do 100% prihvatljivih troškova. Prijavitelji ne trebaju osigurati vlastito sufinanciranje.",
                "● Udruge i zadruge koje imaju utvrđeno djelovanje u području zaštite zdravlja i/ili sigurnosti na radu,\n" +
                        "● Ustanova registrirana za obavljanje djelatnosti u zdravstvu,\n" +
                        "● Jedinice lokalne i područne (regionalne) samouprave (JL(R)S).",
                "● edukacije (radionice, seminari, treninzi)\n" +
                        "● konferencije, kongresi, stručni skupovi u zemlji i inozemstvu\n" +
                        "● okrugli stolovi\n" +
                        "● izrada edukativnih i obrazovnih materijala",
                "",
                "31.12.2020.")
      /*  var civilno4: Contest = createContest(17,
                "Razvoj usluge osobne asistencije za osobe s invaliditetom – faza II",
                "Bespovratna sredstva",
                "Socijalna uključenost",
                "Obustavljen",
                "https://strukturnifondovi.hr/natjecaji/razvoj-usluge-osobne-asistencije-za-osobe-s-invaliditetom-faza-ii/",
                "24.05.2018.",
                "155.000.000,00HRK",
                "● Povećati socijalnu uključenost i unaprijediti kvalitetu života osoba s najtežom vrstom i stupnjem invaliditeta, osoba s intelektualnim teškoćama i mentalnim oštećenjima kroz pružanje usluge osobne asistencije (Skupina 1).\n" +
                        "● Povećati socijalnu uključenost i unaprijediti kvalitetu života gluhim i gluhoslijepim osobama putem usluga tumača/prevoditelja hrvatskog znakovnog jezika (Skupina 2).\n" +
                        "● Povećati socijalnu uključenost i unaprijediti kvalitetu života slijepim osobama putem usluga videćeg pratitelja (Skupina 3).",
                "Otvoreni poziv na dostavu projektnih prijedloga (bespovratna sredstva)",
                "",
                "",
                "",
                "",
                "",
                "",
                "")*/

        var javni1: Contest = createContest(17,
                "Uspostava veteranskih centara u Republici Hrvatskoj",
                "Bespovratna sredstva",
                "Socijalna uključenost",
                "Otvoren",
                "https://efondovi.mrrfeu.hr/MISCms/Pozivi/Poziv?id=4d435049-7296-4f98-a956-f77469218fe3",
                "13.03.2018.",
                "288.800.000,00 HRK",
                "Svrha, odnosno cilj, poziva su radovi dogradnje, rekonstrukcije, obnove i prilagodbe prostora veteranskih centara i nabava opreme za četiri veteranska centra u Šibeniku, Petrinji, Daruvaru i Sinju te u iznimnim slučajevima izgradnja na navedenim lokacijama.",
                "Predmet ovog Poziva je provedba aktivnosti u području infrastrukturnih ulaganja koja će omogućiti rad na povećanju socijalne uključenosti i smanjenju siromaštva ratnih veterana, njihovih obitelji i civilnih žrtava Domovinskog rata.",
                "288.800.000,00 HRK",
                "288.800.000,00 HRK",
                " do 100,00% prihvatljivih troškova",
                "Ministarstvo hrvatskih branitelja.",
                "● Priprema projektne i tehničke dokumentacije (npr. ishođenje potrebnih dozvola i rješavanje imovinsko-pravnih odnosa, idejna rješenja za sve četiri lokacije s pripadajućim detaljnim troškovnicima radova i opreme, izrada studije izvodljivosti i financijske i socio-ekonomske analize za projekt, izrada glavne i izvedbene projektne dokumentacije, izrada ocjene o potrebi procjene utjecaja zahvata na okoliš i sl., revizija građevinskih projekata i ostale dokumentacije potrebne za građevinske radove, projektantski nadzor, projektne prijave itd.).\n" +
                        "● Priprema i objava najave nadmetanja, provođenje postupka javne nabave za izvođenje radova, stručnog nadzora radova i nabavu opreme te potpisivanje ugovora.\n" +
                        "● Radovi na dogradnji, rekonstrukciji, obnovi i prilagodbiobjekata veteranskih centara\n" +
                        "● Izgradnja novih objekata u iznimnim slučajevima, uključujući pripremne radove za izgradnju te povezane aktivnosti (npr. dovođenje komunalnih priključaka, rušenje postojećeg objekta, čišćenje zemljišta, iskolčenje i sl.) na lokacijama četiriju veteranskih centara definiranih u točci 1.3. ovih Uputa za prijavitelje.\n" +
                        "● Stručni nadzor radova.\n" +
                        "● Nabava specijalizirane opreme potrebne za aktivnosti pružanja psiho-socijalnih usluga i rehabilitacije.\n" +
                        "● Nabava ostale opreme (kao što je informatička oprema i namještaj, tehnička oprema za premošćivanje visinskih arhitektonskih razlika…).\n" +
                        "● Edukacija za pružatelje usluga u veteranskim centrima vezana uz korištenje specijalizirane opreme nabavljene u okviru ovog projekta potrebne za aktivnosti pružanja psiho-socijalnih usluga i rehabilitacije koju pruža dobavljač ili podugovoreni stručnjak, i edukacije vezane za provođenje horizontalnih aktivnosti.\n" +
                        "● Upravljanje projektom.\n" +
                        "● Neovisna financijska revizija projekta koju nabavlja korisnik (obvezna za projekte čija ukupna tražena bespovratna sredstva iznose 1.500.000,00 HRK i više).\n" +
                        "● Promotivne aktivnosti s ciljem podizanja vidljivosti projektnih aktivnosti i EU financiranja.",
                "",
                "01.03.2019. godine ili do iskorištenja financijskih sredstava.")

        var javni2: Contest = createContest(18,
                "Energetska obnova i korištenje obnovljivih izvora; referentni broj: KK.04.2.1.04",
                "Bespovratna sredstva",
                "Energija",
                "Otvoren",
                "https://strukturnifondovi.hr/natjecaji/energetska-obnova-koristenje-obnovljivih-izvora-energije-zgradama-javnog-sektora/",
                "16.11.2017.",
                "380.000.000,00 HRK",
                "Smanjenje potrošnje energije u zgradama javnog sektora. Ovim Pozivom podupirat će se mjere energetske obnove koje će rezultirati smanjenjem potrošnje energije za grijanje/hlađenje (QH,nd) na godišnjoj razini (kWh/god) od najmanje 50% u odnosu na godišnju potrošnju energije za grijanje/hlađenje prije provedbe navedenih mjera i korištenje obnovljivih izvora energije.",
                "Otvoreni poziv na dostavu projektnih prijedloga (bespovratna sredstva)",
                "80.000,00 HRK",
                "40.000.000,00 HRK",
                "100%",
                "● Javna tijela (ministarstva i agencije);\n" +
                        "● Jedinice lokalne samouprave;\n" +
                        "● Jedinice regionalne (područne) samouprave;\n" +
                        "● Ustanova socijalne skrbi;\n" +
                        "● Ustanova u kulturi;",
                "I. Projektna dokumentacija\n" +
                        " \n" +
                        "● Priprema projektne dokumentacije – izrada glavnog projekta energetske obnove zgrade i pripadajućih elaborata ako je primjenjivo, ovisno o vrsti građevine, odnosno radova. Glavni projekt i pripadajući elaborati trebaju biti izrađeni prema Zakonu o gradnji (NN 153/13, 20/17), Pravilniku o obveznom sadržaju i opremanju projekata građevina (NN 64/14, 41/15, 105/15, 61/16, 20/17) te ostalim propisima donesenim na temelju Zakona o gradnji i posebnih propisa;\n" +
                        "● Energetski pregled zgrade, izrada izvješća o energetskom pregledu zgrade i energetskog certifikata prije provedene energetske obnove (za zgrade javne namjene u kojima se obavljaju prihvatljive društvene djelatnosti čija ukupna korisna površina ne prelazi 250 m2), koji su izrađeni u skladu sa Zakonom o gradnji (NN 153/13, 20/17) i propisima donesenima na temelju Zakona o gradnji te pravilima struke.\n" +
                        " II. Energetska obnova\n" +
                        " \n" +
                        "● Obnova ovojnice zgrade – povećanje toplinske zaštite ovojnice kojom se dodaju, obnavljaju ili zamjenjuju dijelovi zgrade koji su dio omotača grijanog ili hlađenog dijela zgrade kao što su prozori, vrata, prozirni elementi pročelja, toplinska izolacija podova, zidova, stropova, ravnih, kosih i zaobljenih krovova, pokrova i hidroizolacija;\n" +
                        "● Ugradnja novog visokoučinkovitog sustava grijanja ili poboljšanje postojećeg;\n" +
                        "● Zamjena postojećeg sustava pripreme potrošne tople vode sustavom koji koristi OIE;\n" +
                        "● Zamjena ili uvođenje sustava hlađenja visokoučinkovitim sustavom ili poboljšanje postojećeg;\n" +
                        "● Zamjena ili uvođenje sustava prozračivanja visokoučinkovitim sustavom ili poboljšanje postojećeg;\n" +
                        "● Zamjena unutarnje rasvjete učinkovitijom;\n" +
                        "● Ugradnja fotonaponskih modula za proizvodnju električne energije iz OIE za potrebe ETC-a;\n" +
                        "● Uvođenje sustava automatizacije i upravljanja zgradom;\n" +
                        "● Uvođenje sustava daljinskog očitanja potrošnje energije i vode i sustava kontrolnih mjerila energenata i vode (obveza za projektne prijedloge u kojima se predviđa formiranje novih ETCa);\n" +
                        "● Uvođenje novih naplatnih mjernih mjesta;\n" +
                        "● Horizontalne mjere koje se odnose na provedbu novih elemenata pristupačnosti kojima se omogućava neovisan pristup, kretanje i korištenje prostora u skladu s Pravilnikom o osiguranju pristupačnosti građevina osobama s invaliditetom i smanjene pokretljivosti;\n" +
                        "● Stručni nadzor građenja;\n" +
                        "● Projektantski nadzor, ako je primjenjivo;\n" +
                        "● Usluga koordinatora zaštite na radu tijekom građenja, ako je primjenjivo;\n" +
                        "● Energetski pregled zgrade, izrada izvješća o energetskom pregledu zgrade i energetskog certifikata nakon provedene energetske obnove, koji su izrađeni u skladu sa Zakonom o gradnji (NN 153/13, 20/17) i propisima donesenima na temelju Zakona o gradnji te pravilima struke; Upravljanje projektom i administracija: izrada Obrazaca 1. i 2. Poziva, administracija i tehnička koordinacija, planiranje i izrada dokumentacije za nadmetanje, poslovi financijskog upravljanja i izvještavanje, izrada dokumentacije prema Postupcima nabave za osobe koje nisu obveznici Zakona o javnoj nabavi;\n" +
                        "● Promidžba i vidljivost projekta:",
                "",
                "31.12.2020.")
        var javni3: Contest = createContest(19,
                "ZAŽELI! - Program zapošljavanja žena",
                "Bespovratna sredstva",
                "Socijalna uključenost",
                "Otvoren",
                "https://strukturnifondovi.hr/natjecaji/poziv-dostavu-projektnih-prijedloga-zazeli-program-zaposljavanja-zena/",
                "30.06.2017.",
                "400.900.000,00 HRK",
                "Omogućiti pristup zapošljavanju i tržištu rada ženama pripadnicama ranjivih skupina s naglaskom na teško dostupna, ruralna područja i otoke.",
                "Osnažiti i unaprijediti radni potencijal teže zapošljivih žena i žena s nižom razinom obrazovanja, zapošljavanjem u lokalnoj zajednici koje će ublažiti posljedice njihove nezaposlenosti i rizika od siromaštva, te ujedno potaknuti socijalnu uključenost i povećati razinu kvalitete života krajnjih korisnika.",
                "900.000,00 HRK",
                "10.000.000,00 HRK",
                "100,00% prihvatljivih troškova",
                "● jedinice lokalne i područne (regionalne) samouprave\n" +
                        "● neprofitne organizacije",
                "● zapošljavanje žena iz ciljnih skupina u svrhu potpore i podrške starijim osobama i osobama u nepovoljnom položaju kroz programe zapošljavanja u lokalnoj zajednici;\n" +
                        "● obrazovanje i osposobljavanje žena iz ciljanih skupina koje će pružati potporu i podršku starijim osobama i osobama u nepovoljnom položaju;\n" +
                        "● promidžba i vidljivost;\n" +
                        "● upravljanje projektom i administracija.",
                "",
                "31.12.2020.")
        var javni4: Contest = createContest(20,
                "Sanacija i zatvaranje odlagališta neopasnog otpada; KK.06.3.1.04.",
                "Bespovratna sredstva",
                "Zaštita okoliša",
                "Otvoren",
                "https://strukturnifondovi.hr/natjecaji/sanacija-zatvaranje-odlagalista-neopasnog-otpada/",
                "08.06.2017.",
                "31.12.2018.",
                "Svrha ovog Poziva je sanacija i zatvaranje odlagališta neopasnog otpada, sprječavanje daljnjih negativnih utjecaja na okoliš i ljudsko zdravlje. Sukladno Pravilniku o načinima i uvjetima odlaganja otpada, kategorijama i uvjetima rada za odlagališta otpada (NN 114/15) sanacija odlagališta je aktivnost pod kojom se podrazumijeva izrada projektne i druge dokumentacije sukladno propisima, rješavanje imovinsko pravnih odnosa vezanih za lokaciju odlagališta, postupak ishođenja odobrenja za građenje, izvođenje građevinskih radova koji omogućuju da se odlaganje otpada obavlja sukladno Pravilniku i zatvaranje odlagališta nakon puštanja u rad centra za gospodarenje otpadom ili popunjenja odlagališnih kapaciteta odlagališta.",
                "Sanacija i zatvaranje odlagališta neopasnog otpada koja su prestala s radom, odnosno aktivnim korištenjem, a nakon sanacije i zatvaranja na njima se više ne planira odlagati otpad.",
                "1.875.000,00 HRK",
                "30.000.000,00 HRK",
                "Intenzitet potpore koji se može dodijeliti iznosi najviše 85,00% prihvatljivih troškova po pojedinom projektom prijedlogu",
                "U sklopu ovog Poziva su jedinice lokalne samouprave (gradovi i općine) na čijem se području nalaze odlagališta koja su predmet ovog Poziva, sukladno ZOGO-u, čl. 23., st. 4, u kojem se navodi da je jedinica lokalne i područne (regionalne) samouprave dužna na svom području osigurati uvjete i provedbu propisanih mjera gospodarenja otpadom.",
                "● troškovi radova na sanaciji i zatvaranju odlagališta sukladno ishođenom pravomoćnom aktu na temelju kojeg mogu započeti građevinski radovi (izvedba građevinskih i drugih radova - pripremnih, zemljanih, konstrukterskih, instalaterskih, završnih te ugradnja građevnih proizvoda, opreme ili postrojenja);\n" +
                        "● troškovi stručnog nadzora građenja;\n" +
                        "● troškovi usluge koordinatora zaštite na radu u fazi izvođenja radova (koordinator II);\n" +
                        "● troškovi upravljanja projektom kao troškovi savjetodavnih usluga koje pružaju vanjski konzultanti;\n" +
                        "● troškovi aktivnosti promidžbe i vidljivosti (sukladno točki 6.7. uputa);\n" +
                        "● porez na dodanu vrijednost za koji Prijavitelj nema pravo ostvariti odbitak.",
                "",
                "31.12.2018. ili do iskorištenja financijskih sredstava.")

    /*    var javni5: Contest = createContest(21,
                "title",
                "Bespovratna sredstva",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "")*/

        ALL_CONTESTS.add(msp1)
        ALL_CONTESTS.add(msp2)
        ALL_CONTESTS.add(msp3)
        ALL_CONTESTS.add(msp4)
        ALL_CONTESTS.add(msp5)
        ALL_CONTESTS.add(msp6)
        ALL_CONTESTS.add(msp7)
        ALL_CONTESTS.add(msp8)
        ALL_CONTESTS.add(msp9)
        ALL_CONTESTS.add(msp10)
        ALL_CONTESTS.add(ruralni1)
        ALL_CONTESTS.add(ruralni2)
        ALL_CONTESTS.add(ruralni3)
        ALL_CONTESTS.add(civilno1)
        ALL_CONTESTS.add(civilno2)
        ALL_CONTESTS.add(civilno3)
        //ALL_CONTESTS.add(civilno4)
        ALL_CONTESTS.add(javni1)
        ALL_CONTESTS.add(javni2)
        ALL_CONTESTS.add(javni3)
        ALL_CONTESTS.add(javni4)
        //ALL_CONTESTS.add(javni5)

        maloSrednjePoduzetništvo.contests.add(msp1)
        maloSrednjePoduzetništvo.contests.add(msp2)
        maloSrednjePoduzetništvo.contests.add(msp3)
        maloSrednjePoduzetništvo.contests.add(msp4)
        maloSrednjePoduzetništvo.contests.add(msp5)
        maloSrednjePoduzetništvo.contests.add(msp6)
        maloSrednjePoduzetništvo.contests.add(msp7)
        maloSrednjePoduzetništvo.contests.add(msp8)
        maloSrednjePoduzetništvo.contests.add(msp9)
        maloSrednjePoduzetništvo.contests.add(msp10)

        ruralniRazvoj.contests.add(ruralni1)
        ruralniRazvoj.contests.add(ruralni2)
        ruralniRazvoj.contests.add(ruralni3)


        civilnoDruštvo.contests.add(civilno1)
        civilnoDruštvo.contests.add(civilno2)
        civilnoDruštvo.contests.add(civilno2)
        //civilnoDruštvo.contests.add(civilno4)

        javniSektor.contests.add(javni1)
        javniSektor.contests.add(javni2)
        javniSektor.contests.add(javni3)
        javniSektor.contests.add(javni4)
        //javniSektor.contests.add(javni5)


        CONTESTS.add(maloSrednjePoduzetništvo)
        CONTESTS.add(ruralniRazvoj)
        CONTESTS.add(civilnoDruštvo)
        CONTESTS.add(javniSektor)
    }

    private fun createContest(id: Int,
                              naziv: String,
                              vrstaPoziva: String,
                              sektor: String,
                              status: String,
                              sluzbenaDokumentacija: String,
                              datumPočetkaZaprimanjaPrijava: String,
                              iznosBespovratnihSredstava: String,
                              svrhaPoziva: String,
                              predmetPoziva: String,
                              najnižiIznosBespovratnihSredstava: String,
                              najvišiIznosBespovratnihSredstava: String,
                              intenzitetPotporePoziva: String,
                              prihvatljiviPrijavitelji: String,
                              prihvatljiviTroskovi: String,
                              prihvatljivostProjekta: String,
                              rokZaPrijavu: String): Contest {

        return Contest(id, naziv, vrstaPoziva, sektor, status, sluzbenaDokumentacija, datumPočetkaZaprimanjaPrijava, iznosBespovratnihSredstava, svrhaPoziva, predmetPoziva, najnižiIznosBespovratnihSredstava, najvišiIznosBespovratnihSredstava, intenzitetPotporePoziva, prihvatljiviPrijavitelji, prihvatljiviTroskovi, prihvatljivostProjekta, rokZaPrijavu)
    }

    private fun createContestGoup(id: Int, name: String, image: String): ContestGroup {
        return ContestGroup(id, name, ArrayList(), convertUri(image))
    }

    fun convertUri(uriString: String): Uri {
        return Uri.parse(uriString)
    }

}