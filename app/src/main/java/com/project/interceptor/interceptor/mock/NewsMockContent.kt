package com.project.interceptor.interceptor.mock

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import com.project.interceptor.interceptor.models.News
import org.jetbrains.anko.doAsync
import java.util.ArrayList
import java.io.InputStream
import java.net.URL

object NewsMockContent {

    /**
     * An array of news items.
     */
    val NEWS: MutableList<News> = ArrayList()

    /**
     * Add mock data
     **/
    init {
        addNews(createMockNews(6, "Kako postupati u skladu s GDPR – om?",
                "OBJAVLJENO 19.11.2018 U KATEGORIJI EU VIJESTI",
                "Cilj ove Uredbe koja je poznatija prema svom skraćenom nazivu GDPR je postavljanje novih visokih standarda kod prikupljanja, čuvanja, obrade i dijeljenja osobnih podataka pojedinaca u cilju njihove bolje zaštite. Definirano je niz mjera kojima je precizno određeno tko, u koju svrhu i pod kojim uvjetima smije čuvati i koristiti osobne podatke drugih osoba. Pravo pojedinca da sam odlučuje kome će i u koju svrhu povjeriti svoje osobne podatke najveći je iskorak ove nove pravne regulative.\n" +
                        "\n" +
                        "\n" +
                        "Poduzeća koja se bave aktivnostima istraživanja i razvoja u obavljanju svoje osnovne djelatnosti pružanja usluga, mogu koristiti određene osobne podatke svojih klijenata, ali i tijela koja su zadužena za vrednovanje, provedbu i praćenje sufinanciranih projekata, također mogu koristiti osobne podatke drugih poduzeća (npr. natjecatelja, korisnika i sl.).Osobni podaci koje takva poduzeća i tijela obrađuju u obavljanju svoje djelatnosti uvjetovani su zakonskim propisima nužni za reguliranje ugovornih odnosa.\n" +
                        "\n" +
                        "\n" +
                        "S obzirom na prirodu djelatnosti koja je utemeljena na povjerljivim podacima koji zahtijevaju visok stupanj zaštite, kao i etičnosti kao jedan od osnovnih postulata struke kod ocjene i provedbe projekata, takva poduzeća i institucije u velikoj mjeri već zadovoljavaju novu pravnu regulativu o zaštiti osobnih podataka. \n" +
                        "Svako takvo poduzeće ili institucija trebala bi obaviti detaljnu reviziju podataka i dokumenata i njihovih tokova, postupaka korištenja, te sigurnosno – tehničkih mjera njihove zaštite.\n" +
                        "\n" +
                        "\n" +
                        "\n" +
                        "Preporučljivo je poštivanje sljedećih mjera i pravila pri obradi osobnih podataka:\n" +
                        "\n" +
                        "Prikupljati isključivo osobne podatke koji su nužno potrebni za obavljanje djelatnosti i reguliranje ugovornih odnosa,\n" +
                        "Podatke treba koristiti isključivo za obavljanje poslova koji su ugovoreni,\n" +
                        "Za sve manipulacije podacima treba imati definirane točne procedure obrade i pohrane,\n" +
                        "U slučaju ugrožavanja sigurnosti podataka treba imati točno definirani plan njihovog ponovnog osiguranja,\n" +
                        "Podatke treba čuvati isključivo dok za time postoji potreba iii dok traje zakonska obaveza čuvanja,\n" +
                        "Podatke se nikada ne smije ustupati trećim osobama bez izričitog zahtjeva onoga na koga se podaci odnose,\n" +
                        "Djelatnici informatičke podrške koji brinu o sigurnosti računala moraju biti obvezani na poštivanje svih pravila Uredbe i Zakona, te na tretiranje podataka kao poslovne tajne,\n" +
                        "Potrebno je voditi detaljnu evidenciju svih osobnih podataka sa kojima se raspolaže,\n" +
                        "Svi dokumenti sa podacima trebaju biti zaštićeni. Oni koji fizički postoje u papirnatom obliku trebaju se čuvati zaključani, a podaci u digitalnom obliku čuvaju se kodirani korištenjem 256 bitnog načina kodiranja,\n" +
                        "Treba obavljati redovitu izradu sigurnosnih kopija (backup) podataka na lokalnim računalima,\n" +
                        "Računala na kojima se podaci obrađuju trebaju biti zaštićena profesionalnim antivirusnim i anti-malware programima,\n" +
                        "Računala na kojima se podaci obrađuju trebaju koristiti „vatrozid“ za zaštitu od neželjenih upada,\n" +
                        "Računala na kojima se podaci obrađuju trebaju biti zaštićena pristupnim lozinkama,\n" +
                        "Sve korištene lozinke moraju biti kompleksne i osmišljene prema sigurnosnim standardima IT struke,\n" +
                        "Prostor u kojem se obavlja djelatnost te čuvaju podaci trećih strana, fizički mora biti dobro osiguran.\n" +
                        "Direktivom zagarantirana prava vlasnika podataka u obradi osobnih podataka koji su povjereni su sljedeći:\n" +
                        "\n" +
                        "Pravo na uvid koji osobni podaci se čuvaju i za što se koriste,\n" +
                        "Pravo na dobivanje informacija o svim poduzetim sigurnosnim mjerama u svrhu čuvanja osobnih podataka,\n" +
                        "Pravo na prijenos osobnih podataka kod drugog subjekata,\n" +
                        "Pravo da se u određenim okolnostima zatraži potpuno brisanje svojih osobnih podataka, tj. pravo na zaborav,\n" +
                        "Pravo na informiranost o svakom događaju ugrožavanja sigurnosti Vaših podataka, a koje je ujedno i zakonska obaveza za onoga tko obavlja obradu podataka.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/3dc/cbf/1c0/GDPR-Interceptor-Project-Foto-min__994_1_0_0_landscape.png"))
         addNews(createMockNews(5,
                 "Otvaranje poduzeća - odlična odluka ili potpuni promašaj?",
                 "OBJAVLJENO 14.11.2018 U KATEGORIJI EU VIJESTI",
                 "Postupak otvaranja gospodarskog subjekta, odnosno pravne osobe (obrt, trgovačko društvo i sl.) svodi se na nekoliko dana prikupljanja dokumentacije te relativno jednostavnu administrativnu proceduru nadležnih institucija. Prije donošenja odluke o otvaranju gospodarskog subjekta, nužno je i neophodno provesti analizu tržišta. Upravo će kvalitetno provedena analiza tržišta dati odgovor na pitanje postavljeno u naslovu teksta.\n" +
                         "\n" +
                         "Uvriježeno je pridavati velik značaj tržišnoj analizi, no u našim uvjetima, toj aktivnosti se pridaje nezasluženo malo pažnje. Problem povezan s postupkom analize tržišta identificirali smo prilikom pripreme brojnih projekata. Nedvojbeno možemo zaključiti kako veliki postotak naših poduzetnika zanemaruje postupak analize tržišta ili ignorira njegove rezultate, a što dovodi do loših poslovnih odluka i gubitaka u poslovanju.\n" +
                         "\n" +
                         "Naime, koliko puta se dogodilo da poduzetnik ili osoba koja namjerava pokrenuti vlastito poslovanje izjavi, „Mi nećemo imati konkurenciju“, i to iz razloga što će, na primjer, proizvoditi i prodavati slasticu kakve još nema na domaćem tržištu. Poduzetnicima treba osvijestiti kako će slastičarni s inovativnom slasticom činiti konkurenciju i trgovački lanci ili pekare u kojima se također može pojesti slastica, vjerojatno ne tako kvalitetna, ali cjenovno prihvatljivija. Konkurenciju, vrlo često, ne čine samo i isključivo drugi gospodarski subjekti slične ili iste djelatnosti.\n" +
                         "\n" +
                         "Analizi tržišta treba posvetiti posebnu pozornost, kao i njenim osnovnim elementima. Osnovne dijelove koje bi svaka analiza tržišta trebala sadržavati su:\n" +
                         "\n" +
                         "Analiza tržišta prodaje i ciljne skupine poslovanja,\n" +
                         "Analiza tržišta nabave i ključnih dobavljača,\n" +
                         "Analiza konkurenata na tržištu i njihove snage.\n" +
                         "Uz osnovne elemente svake analize tržišta, istu valja potkrijepiti i slijedećim elementima, a koji će u sinergiji rezultirati mjerodavnijim izlaznim podacima:\n" +
                         "\n" +
                         "Tržišta segmentacija,\n" +
                         "Veličina tržišta,\n" +
                         "Trendovi na tržištu,\n" +
                         "Stope rasta/pada na tržištu,\n" +
                         "SWOT analiza,\n" +
                         "Analiza profitabilnosti tržišne niše,\n" +
                         "Razrada troškova,\n" +
                         "Cjenovna analiza,\n" +
                         "Kanali promocije\n" +
                         "Kanali distribucije,\n" +
                         "Ključni čimbenici uspjeha.\n" +
                         "Bitno je napomenuti kako je kompleksnija analiza tržišta s razrađenim gotovo svim elementima koji definiraju neko tržište, garancija dobivanja relevantnih povratnih informacija. Detaljnom provedbom analize tržišta, s njenim elementarnim dijelovima, možete dobiti relevantnu povratnu informaciju o isplativosti otvaranja vlastite pravne osobe, odnosno uspostave vlastitog poslovanja. Taj postupak može biti kvalitativni i kvantitativni temelj ostvarivanja budućih prihoda ili signalizacija velikih poslovnih gubitaka. Rezultate, kvalitetno provedene analize tržišta, ni u kom slučaju ne treba zanemarivati jer upravo ona služi za preispitivanje Vaše poslovne ideje.\n" +
                         "\n" +
                         "Dakako, analizu tržišta valja provoditi tijekom poslovanja, poglavito prije plasmana neke nove usluge ili proizvoda na tržište jer će ona rezultirati relevantnim podacima o budućoj tržišnoj apsorpciji istih. Analiza tržišta je, u složenijoj ili sažetijoj formi, sastavni dio marketinških studija, poslovnih planova, investicijskih elaborate te projektnih prijedloga za dodjelu bespovratnih sredstava, a što jasno upućuje na njenu važnost prilikom pokretanja investicijskih ulaganja. Obzirom o vlastitim resursima, analizu tržišta, poduzetnik može provoditi samostalno ili za to angažirati financijske stručnjake. Ukoliko ste uložili sredstva u analizu tržišta, a ista je rezultirala lošim rezultatima za Vaše poslovanje, vjerujte, uštedjeli ste sredstva, jer su gubitci koje biste neminovno doživjeli, višestruko veći od troška postupka analize tržišta.",
                 "https://www.interceptorproject.hr/storage/app/uploads/public/103/710/fe4/MSP_InterceptorProject_EUFndovi__994_1_0_0_landscape.png"))
         addNews(createMockNews(7,
                "PROCES ISTRAŽIVANJA I NJEGOVA VAŽNOST",
                "OBJAVLJENO 07.11.2018 U KATEGORIJI EU VIJESTI",
                "Najčešći problem poslovanja mnogih tvrtki je stvaranje i održavanje zdravog poslovanja, koje bi postavilo temelje za rast i razvoj te ostvarivanje pozitivnih rezultata tvrtke. Kako bi se problem dezorijentiranosti u poslovanju mogao izbjeći, te kako bi se uočili segmenti koji zaista utječu na mnogobrojne faktore uspješnog poslovanja, nužno je provesti proces istraživanja tržišta. Takav standardizirani postupak zasnovan na načelima znanstvene metode kojim se prikupljaju, analiziraju i interpretiraju podaci s ciljem dobivanja informacija za optimalno poslovno odlučivanje i tržišno poslovanje uvelike pomažu tvrtkama prilikom odabira prave strategije poslovanja.\n" +
                        "\n" +
                        "Istraživanje tržišta provodi se u nekoliko faza:\n" +
                        "\n" +
                        "Definiranje cilja istraživanja,\n" +
                        "Prikupljanje i vrednovanje podataka,\n" +
                        "Detaljna analiza.\n" +
                        "1. Definiranje cilja istraživanja\n" +
                        "\n" +
                        "\n" +
                        "Kako bi se iz istraživanja tržišta izvukle najkvalitetnije informacije, najbitnije je definirati pravi cilj, ono čemu bi poslovanje težilo. Ovaj korak obuhvaća ciljeve koje tvrtka želi ostvariti prikupljanjem podataka nužnih za identificiranje poslovnih prilika ali i potencijalnih prepreka. U obzir se uzimaju i pretpostavke o faktorima i potencijalnim situacijama u kojima će se poduzeće nalaziti. Kako bi se provela što kvalitetnija analiza potrebno je sastaviti pitanja na koja će se dobiti konkretni odgovori iz rezultata istraživanja.\n" +
                        "\n" +
                        "\n" +
                        "2. Prikupljanje podatka\n" +
                        "\n" +
                        "\n" +
                        "Nakon što smo odredili cilj istraživanja vrijeme je za prikupljanje i vrednovanje istih. Podaci koji će se prikupiti uz pomoć određenih alata svrstavaju se u dvije skupine: primarne i sekundarne. Primarni podaci su oni koji se odnose na samu tvrtku i njeno poslovanje. Ukoliko nas zanimaju podaci vezani uz naše ciljne skupine potrošača, one mogu biti prikupljene uz pomoć alata kao što su ankete, fokus grupe i intervjui. Nadalje, veliki broj informacija može se i prikupiti uz pomoć već provedenih istraživanja od strane trećih osoba ili putem interneta. Takve vrste podataka zovu se sekundarni podaci. Razvoj interneta je doveo do toga da danas na raspolaganju imamo niz alata koji daju veliku slobodu i izbor u načinu prikupljanja podataka, no kod konkretnijih istraživanja koje se više fokusiraju na želje i potrebe potencijalnih potrošača koji bi koristili usluge/proizvode određene tvrtke, vjerodostojnije su informacije koje se dobiju iz primarnih istraživanja za koje je potrebno uložiti više vremena i resursa u njihovu provedbu.\n" +
                        "\n" +
                        "U praksi, sekundarno se istraživanje upravo zbog brzine i znatno manjeg uloga financijskih resursa koristi kao priprema za primarno istraživanje. Uz sve to postoje istraživanja koja obraćaju veću pozornost na vanjske čimbenike. Jedan od najkorištenijih alata za takvu vrstu je PEST analiza. Ono se sastoji od 5 faktora, to su redom: Politički (političko ustrojstvo, zakoni i propisi, pravne promjene), Ekonomski (gospodarska stabilnost, bankovna i porezna politika, opći ekonomski okvir), Sociološko-demografski (postojeći društveni stavovi, stupanj obrazovanja stanovništva) i Tehnološki (dostupnost adekvatnih tehnologija). Uz pomoć takve analize poduzeća pokušavaju zaobilaziti vanjske prepreke koje bi ih eventualno mogle spriječiti do postizanja određenog poslovnog cilja.\n" +
                        "\n" +
                        "Ovisno o definiranim ciljevima istraživanja te prikupljenim podacima, dalje se mogu raditi analize koje bi kreirale „pobjedničku“ strategiju poduzeća. Ukoliko je istraživanje pogodilo suštinu, te su podaci precizni i kvalitetno obrađeni, dalje se mogu koristiti u analizama pomoću kojih se kreira strategija poslovanja i na koncu konca dolazi do uspjeha u poslovanju poduzeća.\n" +
                        "\n" +
                        "\n" +
                        "3. Detaljna analiza\n" +
                        "\n" +
                        "\n" +
                        "Nakon što smo prikupili i vrednovali podatke, vrijeme je za njihovu analizu. Od svih prikupljenih informacija tražit će se one koje su relevantne za upoznavanje potreba i segmentaciju tržišta. Takve informacije mogu biti na primjer, kupovna moć potencijalnih klijenata, veličina tržišne niše, cijena i uvjeti prodaje proizvoda/usluge konkurencije, uvjeti rada s pojedinim dobavljačima, zakonodavstvo i standardi te kvaliteta, cijena i dostupnosti adekvatne tehnologije.\n" +
                        "\n" +
                        "\n" +
                        "Istraživanje tržišta laički može biti pojednostavljeno kao detektiranje ključnih segmenata koje bi pomogle kreiranju zdravog i pozitivnog poslovanja određene tvrtke. Vodeći se time, od iznimne je važnosti detaljno istražiti tržište prodaje, a sve to kako bi uočili kakva je situacija na tržištu, što je to tržištu zaista potrebno i gdje se naši proizvodi ili usluge nalaze na tome tržištu. Nakon toga slijedi proučavanje konkurencije. Važnost u detaljnom analiziranju konkurencije nalazi se kod pozicioniranja određenih proizvoda ili usluga na tržište. Bitno je koliko su konkurenti snažni, koju dodatnu vrijednost mogu ponuditi kupcu, te na koji način se može takvoj konkurenciji preuzeti dio kupaca. Važno je napomenuti da bez kvalitetno odrađena prethodna dva segmenta istraživanja tržišta, detaljnu analizu je vrlo teško kvalitetno odraditi, a i ako se odradi, upitna je relevantnost i korisnost takvih podataka koji bi trebali pomoći poduzeću prilikom kreiranja pobjedničke strategije.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/50b/9ea/e5c/istrazivanje-razvojtrzisteinterceptorprojectfoto__994_1_0_0_landscape.jpg"))
         addNews(createMockNews(8,
                "Prvi korak prema povoljnim kreditima za poljoprivrednike!",
                "OBJAVLJENO 30.10.2018 U KATEGORIJI EU VIJESTI",
                "Obzirom na primjetnu medijsku kampanju u sklopu koje se promovira dodjela povoljnih kreditnih sredstava, pojavili su se brojni upiti na temu postupka dodjele i koraka koje je potrebno poduzeti kako bi poljoprivrednik dobio ista.\n" +
                        "\n" +
                        "Financijski instrumenti „Mikro zajam za Ruralni razvoj“ i „Mali zajam za Ruralni razvoj“ objavljeni su u skladu s Programom ruralnog razvoja Republike Hrvatske za razdoblje 2014.-2020. i Sporazumom o financiranju potpisanog 18. travnja 2018. godine između Ministarstva poljoprivrede, Agencije za plaćanja u poljoprivredi, ribarstvu i ruralnom razvoju i Hrvatske agencije za malo gospodarstvo i investicije (HAMAG-BICRO).\n" +
                        "\n" +
                        "Zahtjevi za dodjelu povoljnih kreditnih sredstava upućuju se agenciji, HAMAG – BICRO, koja ih po primitku pregledava te donosi konačne odluke o dojeli povoljnih kreditnih sredstava, odnosno financiranju projekta.\n" +
                        "\n" +
                        "Prije svega bitno je istaknuti početni korak u postupku dodjele povoljnih kreditnih sredstava. Naime, poljoprivrednici iz brojnih sektora nerijetko se za dodjelu povoljnih kreditnih sredstava obraćaju direktno Hrvatskoj agenciji za malo gospodarstvo i investicije (HAMAG-BICRO). Problem je evidentan i kod komercijalnih banaka koje učestalo primaju veliki broj upita za kreditnim sredstvima. Kako bi se poljoprivredniku, neovisno o sektoru, dodijelila povoljna kreditna sredstva, on mora izraditi projektnu dokumentaciju. Ukoliko poljoprivrednik posjeduje dovoljno znanja i kompetencija za pripremu cjelokupne projektne dokumentacije za dodjelu kreditnih sredstava, pa tako i onih povoljnih, može ju izraditi samostalno.\n" +
                        "\n" +
                        "Obzirom da su ljudi iz sektora poljoprivrede zaokupljeni vlastitim poslom i obvezama na poljoprivrednom gospodarstvu, jasno je kako oni uglavnom nemaju vremena izrađivati projektnu dokumentaciju. U tu svrhu, poljoprivrednici se trebaju obratiti stručnjacima za pripremu projektne dokumentacije ili konzultantima koji posjeduju potrebna ekonomska znanja.\n" +
                        "\n" +
                        "Zaključujemo, prvi korak poljoprivrednika ka dobivanju povoljnih kreditnih sredstava jest kontaktiranje stručnjaka za pripremu projektne dokumentacije. Dakako, na domaćem tržištu posluje veliki broj konzultantskih poduzeća koji pružaju uslugu izrade projektne dokumentacije, stoga je bitno napraviti pravi odabir i pronaći pouzdanog partnera koji će znati osigurati potrebna sredstva. Stručnjaci za izradu projektne dokumentacije ili konzultanti su osobe koje su uz Vas prilikom pripreme, a pogotovo prilikom realizacije povoljnih kreditnih sredstava. Po potpisivanju ugovora o dodjeli povoljnih kreditnih sredstava pri HAMAG-BICRO – u, konzultanti surađuju s korisnicima kako bi se dodijeljena sredstva namjenski, pravilno i transparentno utrošila.\n" +
                        "\n",
                "https://www.interceptorproject.hr/storage/app/uploads/public/fa5/e43/28d/ESIF-Zajam-Interceptro-project_Foto__994_1_0_0_landscape.jpg"))
         addNews(createMockNews(9,
                "Postupak dodjele povoljnih kreditnih sredstava",
                "OBJAVLJENO 25.10.2018 U KATEGORIJI EU VIJESTI",
                "Kako je spomenuto u prethodnom članku, „Prvi korak prema povoljnim kreditima za poljoprivrednike!“, za pripremo i realizaciju povoljnih kreditnih sredstava u sklopu financijskih instrumenata, „Mikro zajam za Ruralni razvoj“ i „Mali zajam za Ruralni razvoj“ koje dodjeljuje Hrvatska agencija za malo gospodarstvo i investicije (HAMAG-BICRO), poljoprivrednicima će nužno biti potrebna pomoć stručnjaka za izradu projektne dokumentacije, odnosno konzultanata.\n" +
                        "\n" +
                        "Kako biste dobili bolji uvid u sam način dodjele povoljnih kreditnih sredstava navodimo informativni primjer:\n" +
                        "\n" +
                        "„Proizvođač mlijeka koji svoju djelatnost vrši u svojstvu Obrta kontaktira konzultantsko poduzeće koje će mu pomoći pri pripremi cjelokupne projektne dokumentacije te realizaciji povoljnih kreditnih sredstava koje dodjeljuje Hrvatska agencija za malo gospodarstvo i investicije (HAMAG-BICRO). Konzultant upoznaje poduzetnika s uvjetima i kriterijima natječaja te zajedno dogovaraju najprihvatljiviji način financiranja projekta.\n" +
                        "\n" +
                        "Proizvođač mlijeka u sklopu investicije nabavlja suvremeni jednoredni sustav izmuzišta te financira bruto primanja zaposlenika za dva mjeseca poslovanja. Vrijednost investicije je 200.000,00 HRK bez uračunatog PDV – a (Poreza na dodanu vrijednost) koji će u investiciji biti trošak prijavitelja, odnosno proizvođača mlijeka.\n" +
                        "\n" +
                        "Zaključujemo, prijavitelj će aplicirati za dodjelu povoljnog kredita u visini od 200.000,00 HRK, dok će u cjelokupnoj investiciji sudjelovati s vlastitim sredstvima u visini od, 50.000,00 HRK, i to u svrhu financiranja PDV – a koji je povrativ. Porez na dodanu vrijednost je u sklopu projekata koji su na neki način financirani iz Europskih strukturnih i investicijskog fondova, uglavnom nije prihvatljiva stavka troška. Nakon definiranja vrijednosti investicije, u dogovoru s konzultantom, a u skladu s realnim načinima kreditiranja, usuglašavaju se uvijati.\n" +
                        "\n" +
                        "Povoljna kreditna sredstva će biti tražena na period od 6 godina, s uračunatim počekom u trajanju od 6 mjeseci te rokom korištenja kredita od dva mjeseca. Sredstva će se vraćati s uistinu povoljnom kamatnom stopom od 0,1%, i to u obliku kvartalnih rata.\n" +
                        "\n" +
                        "Potom stručnjaci za izradu projektne dokumentacije izrađuju projektnu dokumentaciju, a prijavitelj prikuplja i šalje statusnu i financiju dokumentaciju svojeg poduzeća. Po izradi i prikupljanju cjelokupne dokumentacije, ista se urudžbira pri Urudžbenom uredu Hrvatske agencije za malo gospodarstvo i investicije (HAMAG-BICRO), a prijavitelju se dodjeljuje referent koji obrađuje zahtjev. Zahtjev za dodjelu povoljnih kreditnih sredstava u sklopu instrumenata, „Mikro zajam za Ruralni razvoj“ i „Mali zajam za Ruralni razvoj“ obrađuje se bez naplaćivanja naknade za obradu.\n" +
                        "\n" +
                        "Obrada zahtjeva potraje ovisno o broju zahtjeva zaprimljenih u određenom periodu. Po donošenju odluke o dodjeli povoljnih kreditnih sredstava, ugovaraju se sredstva osiguranja kredita. Natječajem je propisano kako će se u tu svrhu tržiti zadužnice poduzeća prijavitelja, u ovom slučaju Obrta, te vlasnika istoga. Ukoliko referenti HAMAG – BICRO – a procjene veći rizik realizacije kreditnih sredstava, mogući su i drugi oblici osiguranja poput hipoteke. Po ugovaranju sredstva osiguranja, prijavitelj potpisuje ugovor o dodjeli povoljnih kreditnih sredstava te kreće u realizaciju investicije. U svrhu pravilnog utroška dodijeljenih sredstava, prijavitelj se može, odnosno treba savjetovati s konzultantom koji je bio odgovoran za pripremu projektne dokumentacije“\n" +
                        "\n" +
                        "Priloženi primjer je isključivo informativnog karaktera te ima svrhu upoznati potencijalne prijavitelje s načinom dodjele povoljnih kreditnih sredstava pri Hrvatskoj agenciji za malo gospodarstvo i investicije (HAMAG-BICRO).",
                "https://www.interceptorproject.hr/storage/app/uploads/public/175/291/857/Foto_ESIF-Zajama-Poljoprivreda-Interceptor-Project__994_1_0_0_landscape.jpg"))
         addNews(createMockNews(10,
                "Sigurnost projektnih informacija",
                "OBJAVLJENO 24.10.2018 U KATEGORIJI EU VIJESTI",
                "Nagli rast i široka primjena digitalnih tehnologija te pojava elektroničkog poslovanja paralelno s pojavom međunarodnog terorizma iznjedrila je potrebu za boljim načinima zaštite računala i informacija koje ona pohranjuju, obrađuju i razmjenjuju. Stoga u današnje doba sigurnost računalnih sustava postaje akademska disciplina unutar raznih profesionalnih organizacija, radeći na zajedničkom cilju osiguranja zaštite i sigurnosti informacijskih sustava.\n" +
                        "\n" +
                        "ISO/IEC 27001 je ISO norma koja je krajem 2005 godine naslijedila staru britansku normu BS 7799-2. Radi se o standardu koji se bavi sustavom upravljanja informacijskom sigurnošću, a namijenjen je za korištenje zajedno s ISO/IEC 27002, prije poznat kao ISO/IEC 17799, što je praktični kodeks koji definira ciljeve sigurnosnih kontrola i preporuča određeni praktični raspon istih. On pruža praktičan model za uspostavljanje, primjenu, korištenje, praćenje, održavanje i konstantno poboljšavanje sustava za upravljanje informacijskom sigurnošću, pri čemu su dizajn i primjena pod utjecajem ciljeva poduzeća ili organizacije, procesa koji se odvijaju, te veličine i strukture same organizacije. One organizacije koje u svom svakodnevnom poslovanju koriste ISO/IEC 27002 pri procjeni svog sustava upravljanja informacijskom sigurnošću vrlo vjerojatno će biti u sukladnosti sa normama ISO/IEC 27001.\n" +
                        "\n" +
                        "Uspostavljanje sustava upravljanja informacijskom sigurnošću se obično sastoji od tri osnovne međusobno povezane faze:\n" +
                        "\n" +
                        "Provjera postojanja i potpunosti ključne dokumentacije potrebne za uvođenje sustava upravljanja informacijskom sigurnošću, a radi se o sigurnosnoj politici organizacije, izjave o primjenjivosti kontrola (Statement of Applicability – SoA) te planu tretmana rizika (Risk Treatment Plan).\n" +
                        "Dubinsko snimanje i revizija koji testiraju postojanje i efikasnost kontrola informacijske sigurnosti koje se navode u izjavi o primjenjivosti kontrola te planu tretmana rizika, uključujući potpornu dokumentaciju. Sam proces procjene i upravljanja rizikom informacijske sigurnosti je kontinuiran, a ne jednokratan proces, baš kao što se teoretski smatra da sustav upravljanja informacijskom sigurnošću nikada nije u potpunosti uveden nego se radi o konstantnom procesu evaluacije te traženja mogućih nesukladnosti i njihovog ispravljanja.\n" +
                        "Treća faza je ponovljena procjena, odnosno naknadna revizija, kojom se provjerava jesu li poduzeće ili organizacija koji su prvobitno certificirani u skladu sa standardom i dalje u sukladnosti s istim. Ona se odvija periodički kako bi se potvrdilo da sustav upravljanja informacijskom sigurnošću funkcionira kako je zamišljeno i dokumentirano.\n" +
                        "Sam standard strukturiran je tako da ga je moguće uvesti u svaku organizaciju, bila ona profitna ili neprofitna te neovisno o njenoj veličini. U samoj srži standarda podržan je osnovni zahtjev informacijske sigurnosti izražen u već opisanoj C-I-A trijadi , prema kojemu je dohvat informacijama dozvoljen samo onima koji su autorizirani za njihovo korištenje, da su one točne i potpune te da je pristup informacijama neprekinut, odnosno dozvoljen u kontinuitetu kada je to potrebno.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/310/ca2/bbe/Foto_Sigurnost%20podataka_Interceptor_Project__994_1_0_0_landscape.png"))
         addNews(createMockNews(11,
                "Može li PDV biti predmet financiranja u sklopu projekata sufinanciranih iz fondova Europske unije?",
                "OBJAVLJENO 20.03.2018 U KATEGORIJI EU VIJESTI",
                "Poduzetnici koji posluju u realnom sektoru nemaju pravo financirati trošak PDV – s na nabavljene stavke u sklopu projekata sufinanciranih bespovratnim sredstvima iz Europske unije. Poduzetnici iz realnog sektora u suradnji sa svojim konzultantima, u projektne prijedloge, uvrštavaju neto vrijednosti stavki kojih nabavljaju.\n" +
                        "\n" +
                        "Drugim riječima, poduzetnik koji se prijavljuje za dodjelu bespovratnih sredstava, a kojemu su ista i odobrena, podnijeti će trošak vlastitog učešća u neto vrijednosti projekta te trošak PDV – a na nabavljene stavke. Podsjećamo, bespovratna sredstva se u realnom sektoru nikada ne dodjeljuju u stopostotnom ili potpunom iznosu stoga poduzetnik uvijek snosi jedan dio vrijednosti investicije,\n" +
                        "\n" +
                        "Temeljem spomenutoga, ponavljamo, u sklopu projekata sufinanciranih bespovratnim sredstvima Europske unije, poduzetnik prijavitelj snosi trošak jednog dijela investicije te PDV – a na stavke koje se nabavljaju.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/201/949/43a/interceptor_1024x600_5__994_1_0_0_landscape.jpg"))
         addNews(createMockNews(12,
                "Bespovratna sredstva za poduzetnike",
                "OBJAVLJENO 01.03.2018 U KATEGORIJI EU VIJESTI",
                "Poduzetnici koji se nisu prijavljivali za dodjelu bespovratnih sredstava nisu adekvatno upoznati s principom korištenja sredstva iz EU fondova. Naime, nedovoljna upoznatost s tematikom bespovratnih sredstava se ponajprije ogleda u intenzitetu potpore samih projekata, odnosno stope sufinanciranja projektnih prijedloga. Dakako, u javnosti nije kvalitetno obrađena spomenuta tema stoga poduzetnici prije prve prijave na natječaj za dodjelu bespovratnih sredstava nerijetko računaju na stopostotni ili potpuni iznos sredstava za svoje projekte.\n" +
                        "\n" +
                        "Poduzetnici koji posluju u realnom sektoru, za svoje projekte mogu dobiti određenu stopu sufinanciranja projekata. Stopa sufinanciranja projektnih prijedloga koji se financiraju iz fondova EU ovisi o razvijenost područja gdje je poduzeće osnovano te o statusu samog poduzeća prijavitelja.\n" +
                        "\n" +
                        "Poduzeća koja su osnovana na manje razvijenim područjima, benefit prijave na Javne Pozive za dodjelu bespovratnih sredstava ostvarit će većom stopom sufinanciranja ili će radi toga ostvariti veći broj ostvarenih bodova prilikom procjene kvalitete projektnih prijedloga, tzv. Evaluacije projektnih prijedloga.\n" +
                        "\n" +
                        "Razvijenost područja određuje se prema vrijednosti indeksu razvijenosti jedinica lokalne ili regionalne samouprave koje su sistematizirane u grupe. Sistematizaciju možete provjeriti na linku: https://narodne-novine.nn.hr/clanci/sluzbeni/2013_12_158_3313.html.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/076/5af/050/Foto_Money_Interceptor-min__994_1_0_0_landscape.jpg"))
        /* addNews(createMockNews(14,
                "title",
                "datum",
                "content",
                "link"))*/
        /* addNews(createMockNews(15,
               "title",
               "datum",
               "content",
               "link"))*/
        /* addNews(createMockNews(16,
               "title",
               "datum",
               "content",
               "link"))*/
        addNews(createMockNews(0, "Uvjeti kreditiranja – kamatna stopa", "OBJAVLJENO 09.07.2018 U KATEGORIJI EU VIJESTI",
                "Obzirom na revitalizaciju gospodarskog sektora što je usko povezano s izvorima investicijskog kapitala, objavljujemo kratki osvrt na kredite, odnosno zajmove koji se trenutno nude na domaćem tržištu.\n" +
                        "\n" +
                        "Tijekom partnerskog i samostalnog djelovanja ustanovili smo kako naši poduzetnici nerijetko ne vladaju osnovnim terminima te izračunima vezanim za kreditna sredstva. Naime, često se stavlja naglasak na što nižu kamatnu stopu i što duži rok otplate. Ovom prilikom ćemo se osvrnuti isključivo na kamatnu stopu za kredite manjih iznosa, do 50.000,00 Eura.\n" +
                        "\n" +
                        "U komercijalnim bankama je moguće dobiti prosječnu kamatnu stopu od 5,00% na spomenuti iznos kredita, dok je u sklopu državnih potpora moguće dobiti kamatnu stopu od 1,50% što je trenutno najpovoljnije na tržištu.\n" +
                        "\n" +
                        "U konkretnom slučaju, na traženi iznos zajma u visini od 50.000,00 Eura uz kamatnu stopu od 5,00% te rok otplate od 5 godina s uračunatim počekom od 6 mjesec, iznos mjesečne rate iznosit će otprilike 7.076,71 HRK. Za iste definirane uvjete, ali s kamatnom stopom od 1,50%, iznos mjesečne rate bi iznosi otprilike 6.491,21 HRK. Zaključujemo, razlika je na razini mjeseca gotovo 600,00 HRK. Nakon otplate, korisnik kredita bi platio 30.000,00 HRK do 40.000,00 HRK više u slučaju nešto nepovoljnijih uvjeta (kamatna stopa od 5,00%).\n" +
                        "\n" +
                        "Poduzeće koje namjerava uzeti kredit mora podnijeti kreditne obveze u oba spomenuta slučaja, a ukoliko je povrat kreditnih sredstava upitan pod nepovoljnijim uvjetima, likvidnost i profitabilnost poslovanja su ugroženi. Poduzetnici bi u tom slučaju trebali proanalizirati svoju poslovnu odluku. Zaključujemo, što je traženi iznos kredita manji, utjecaj kamatne stope je manje osjetan. S druge strane, krediti se u komercijalnim bankama kudikamo brže realiziraju što je poduzetnicima katkad izrazito važno jer su im sredstva nerijetko hitno potrebna.\n" +
                        "\n" +
                        "Treba biti oprezan i prilikom predviđanja roka otplate kredita te imati na umu da će se kroz dulji rok otplate, dulje otplaćivati kamata, bez obzira što se ona smanjuje. Mjesečna rata će se smanjiti ukoliko se produlji rok otplate, ali će se u konačnici platiti veća kamata nego u slučaju kraćeg roka otplate.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/35f/797/219/pokazatelji_javnog_poziva__994_1_0_0_landscape.jpg"))
        addNews(createMockNews(1,
                "Može li PDV biti predmet financiranja u sklopu projekata sufinanciranih iz fondova Europske unije?",
                "OBJAVLJENO 20.03.2018 U KATEGORIJI EU VIJESTI",
                "Poduzetnici koji posluju u realnom sektoru nemaju pravo financirati trošak PDV – s na nabavljene stavke u sklopu projekata sufinanciranih bespovratnim sredstvima iz Europske unije. Poduzetnici iz realnog sektora u suradnji sa svojim konzultantima, u projektne prijedloge, uvrštavaju neto vrijednosti stavki kojih nabavljaju.\n" +
                        "\n" +
                        "Drugim riječima, poduzetnik koji se prijavljuje za dodjelu bespovratnih sredstava, a kojemu su ista i odobrena, podnijeti će trošak vlastitog učešća u neto vrijednosti projekta te trošak PDV – a na nabavljene stavke. Podsjećamo, bespovratna sredstva se u realnom sektoru nikada ne dodjeljuju u stopostotnom ili potpunom iznosu stoga poduzetnik uvijek snosi jedan dio vrijednosti investicije,\n" +
                        "\n" +
                        "Temeljem spomenutoga, ponavljamo, u sklopu projekata sufinanciranih bespovratnim sredstvima Europske unije, poduzetnik prijavitelj snosi trošak jednog dijela investicije te PDV – a na stavke koje se nabavljaju.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/4b8/a24/783/interceptor_1024x600_1__994_1_0_0_landscape.jpg"))
        addNews(createMockNews(2,
                "Tko se može prijaviti na natječaj za dodjelu bespovratnih ili kreditnih sredstava?",
                "OBJAVLJENO 14.03.2018 U KATEGORIJI EU VIJESTI",
                "Prvi i osnovni preduvjet za prijavu za dodjelu bespovratnih ili kreditnih sredstava jest imati neki oblik pravne osobe ili gospodarskog subjekta poput obrta, trgovačkog društva i slično. Prihvatljivost prijavitelja na određeni natječaj definiran je samim natječajem pa možemo slobodno konstatirati kako su za natječaje ruralnog razvoja prihvatljivi obrti, OPG –ovi, i trgovačka društva, za natječaje raspisane za dodjelu sredstva iz Europskog socijalnog fonda prihvatljive su udruge, civilne organizacije i ustanove, dok su za infrastrukturne natječaje prihvatljive jedinice lokalne i regionalne samouprave. Zaključujemo, natječajnom dokumentacijom je propisan pravni oblik prijavitelja koji je prihvatljiv u sklopu istoga.\n" +
                        "\n" +
                        "Neovisno o pravnom obliku prijavitelja, definirani su neki kriteriji koji moraju biti gotovo uvijek ispunjeni. Naime, u trenutku prijave na Javni Poziv, prijavitelj ne smije imati nikakvog poreznog dugovanja, prijavitelj ne smije biti u postupku stečajne nagodbe, likvidacije ili u blokadi, prijavitelj ne smije primiti potporu ukoliko je u predmetnoj te prethodne dvije fiskalne godine poslovanja primio više od 200.000 Eura potpore, kao ni u slučaju da se protiv njega vodi kazneni postupak.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/902/11a/9f6/Foto_Aplicant_Project-min__994_1_0_0_landscape.jpg"))
        addNews(createMockNews(3,
                "Što su osnovna, a što obrtna sredstva u poslovanju?",
                "OBJAVLJENO 07.03.2018 U KATEGORIJI EU VIJESTI",
                "Osnovna i obrtna sredstva svakodnevna su pojava u poslovanju poduzeća no nerijetko se događa da ih poduzetnici u svojem vlastitom poslovanju. Predmetna tema nije krucijalno važna za poslovanje poduzeća no razumijevanje iste će biti od koristi ukoliko će poduzetnik tražiti bespovratna sredstva, kreditna sredstva, jamstva i sl.\n" +
                        "\n" +
                        "Osnovna sredstva\n" +
                        "Osnovna sredstva su ona sredstva koja su poduzetniku neophodna za vršenje poslovanja, koriste se u dužem vremenskom razdoblju, podliježu obračunu amortizacije, odnosno otpisu vrijednosti robe te se ubrajaju u dugotrajnu imovinu nekog poduzeća. Osnovna sredstva mogu biti stečena kupnjom, izgradnjom ili izradom, razmjenom, darivanjem i slično. Obzirom na navedeno, zaključujemo, računala i računalna oprema osnovna su sredstva poduzeću za razvoj aplikacija, CNC i drugi strojevi, osnovna su sredstva poduzećima koja se bave proizvodnjom ili preradom, a apartman je osnovno sredstvo poduzeću koje se bavi iznajmljivanjem smještajnih kapaciteta.\n" +
                        "\n" +
                        "Obrtna sredstva\n" +
                        "Obrtna sredstva su ona sredstva imovina kojom raspolaže poduzeće, a koja traje manje od jedne godine i koja je uključena u kratkotrajnu imovinu poduzeća. Temeljem spomenutog, obrtna sredstva mogu biti sirovine koje se koriste za proizvodnju, plaće zaposlenika, najam poslovnog prostora, potrošni uredski materijal i slično.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/5b8/3a3/8d9/Foto_Success_Project-min__994_1_0_0_landscape.jpg"))
        addNews(createMockNews(4,
                "Evaluacija projektnih prijava",
                "OBJAVLJENO 02.03.2018 U KATEGORIJI EU VIJESTI",
                "Prilikom pripreme projektne dokumentacije izrazito je važno obratiti pozornost na kvalitetu izrade. Kvaliteta projektnog prijedloga osnova je za odabir onih projekata koji će biti sufinancirani sredstvima iz Europske unije.\n" +
                        "\n" +
                        "Kriteriji za procjenu ili evaluaciju projektnih prijedloga definirani su za svaki Javni Poziv objavljen za dodjelu bespovratnih sredstava. Kriteriji koji mogu biti temelj evaluacije mogu biti: vrijednosti natječajem definiranih pokazatelja, održivost rezultata poslovanja, stručnost vlasničke strukture i zaposlenika poduzeća, doprinos horizontalnim temama i sl. Javnim Pozivom je definiran i broj bodova koji se dodjeljuje obzirom na ispunjenost definiranih kriterija, a čiji zbroj mora biti veći od najnižeg definiranog bodovnog praga kako bi projekt bio sufinanciran iz fondova EU. Zaključujemo, kriteriji za procjenu kvalitete projektnih prijedloga su različiti i o ispunjenu istih direktno ovisi odluka o dodjeli bespovratnih sredstava.\n" +
                        "\n" +
                        "Temeljem navedenog, u suradnji s konzultantima, pokušajte napraviti projektni prijedlog koji će maksimalno moguće udovoljavati kriterijima definirani Javnim Pozivom.",
                "https://www.interceptorproject.hr/storage/app/uploads/public/e22/a7a/5d9/interceptor_1024x600_2__994_1_0_0_landscape.jpg"))
       /* addNews(createMockNews(5,
                "title",
                "datum",
                "content",
                "link"))*/
    }

    private fun addNews(item: News) {
        NEWS.add(item)
    }

    private fun createMockNews(id: Int, title: String, date: String, content: String, image: String): News {
                return News(id, title, date, content, image )
    }


    private fun convertUri(uriString: String): Uri {
        return Uri.parse(uriString)
    }



}