package com.project.interceptor.interceptor.models


enum class HomePageTab(val tab: Int) {
    NEWS(0),
    CONTESTS(1)
}