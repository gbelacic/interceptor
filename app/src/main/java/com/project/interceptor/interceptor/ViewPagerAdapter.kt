package com.project.interceptor.interceptor

import android.content.Context
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup

public class ViewPagerAdapter(fm: FragmentManager?, val mContext: Context) : FragmentPagerAdapter(fm) {
    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        var homeFragment: HomeFragment = HomeFragment()
        var bundle: Bundle = Bundle()
        bundle.putString("message", "Fragment : " + position)


        homeFragment.arguments = bundle

        //return  homeFragment

        when(position){
            0 -> return createNewsFragment()
            1 -> return createContestsFragment()
            else -> return homeFragment
        }
    }



    override fun getPageTitle(position: Int): CharSequence? {

        when(position){
            0 -> return "Novosti"
            1 -> return  "Natječaji"
            else -> return null
        }
    }

    fun createNewsFragment(): Fragment{
        var newsFragment: NewsFragment = com.project.interceptor.interceptor.NewsFragment()
        var bundle: Bundle = Bundle()
        bundle.putString("message", "Ovo su novosti")

        newsFragment.arguments = bundle

        return newsFragment
    }

    fun createContestsFragment(): Fragment{
        var contestsFragment: ContestsFragment = com.project.interceptor.interceptor.ContestsFragment(mContext)
        var bundle: Bundle = Bundle()
        bundle.putString("message", "Ovo su natječaji")

        contestsFragment.arguments = bundle

        return contestsFragment
    }




}