package com.project.interceptor.interceptor.mock

object AboutUs {

    var ABOUT_US: String = String()

    init {
        ABOUT_US = "Mobilna aplikacija, “INTERCEPTOR PODUZETNIČKI VODIČ”, osmišljena je i napravljena kako bi se poduzetnicima, onima koji će to tek postati te ostaloj zainteresiranoj javnosti omogućilo jednostavno i pravovremeno informiranje o izvorima kapitala na tržištu te o povezanim temama. Koristeći aplikaciju,  “INTERCEPTOR PODUZETNIČKI VODIČ”, pravovremeno ćete dobivati informacije vezane za nove natječaje za sufinanciranje projekata putem bespovratnih sredstava, te putem drugih oblika financiranja poput financijskih instrumenata ili vaučera. Dakako, aplikacija nudi i široki pregled investicijskih kreditnih programa koje plasiraju komercijalne banke na domaćem tržištu.\n" +
                "\n" +
                "U sklopu aplikacije se filtriraju točno one informacije koje su posjetiteljima relevantne za određenu temu ili natječaj, dok se detaljnije informacije mogu dobiti putem kontakt obrasca ili osobnog sastanka s financijskim stručnjacima.\n" +
                "\n" +
                "U svakom trenutku možete jednostavno i praktično potražiti informacije vezane za nove natječaje za bespovratna sredstva, povoljna kreditna sredstva i ostale izvore investicijskog kapitala te uvjete i kriterije koji su vezani za njihovu dodjelu. Aplikacija ima i svoj savjetodavni karakter te sadrži bazu obrađenih tema koji mogu biti odličan izvor informacija zainteresiranim posjetiteljima. Aplikacija Vam omogućuje postavljanje upita na određenu temu, a odgovor će biti objavljen u sklopu aplikacije te će te o objavi biti obaviješteni putem E – mail adrese. \n" +
                "\n" +
                "Oglašavanje putem mobilne aplikacije, “INTERCEPTOR PODUZETNIČKI VODIČ”, moguće je ugovoriti putem kontakt obrasca, kao i sve ostale povezane uvjete.\n"
    }
}