package com.project.interceptor.interceptor.ui

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.Image
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.firebase.FirebaseApp
import com.project.interceptor.interceptor.R
import com.project.interceptor.interceptor.mock.NewsMockContent
import com.project.interceptor.interceptor.models.News
import kotlinx.android.synthetic.main.activity_main.view.*
import com.google.android.gms.ads.AdView


class NewsActivity : AppCompatActivity() {

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)


        FirebaseApp.initializeApp(this)
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713")
        /* set status bar to black instead od of default background color*/
        window.statusBarColor = Color.BLACK


        var mAdView = findViewById<AdView>(R.id.adView) as AdView

        val adRequest = AdRequest.Builder().tagForChildDirectedTreatment(true).build()

        mAdView.loadAd(adRequest)



        mAdView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            override fun onAdFailedToLoad(errorCode: Int) {
                // Code to be executed when an ad request fails.
            }

            override fun onAdOpened() {
                openNewTabWindow("https://www.interceptorproject.hr/")
            }

            override fun onAdLeftApplication() {
                Log.e("left", "apk")
            }

            override fun onAdClosed() {
                mAdView.visibility = View.INVISIBLE
                Log.e("close", "ad")
            }
        }


        val bundle = intent.extras

        if (bundle != null) {
            val newsId: Int = bundle.getInt("news_id")
            val news: News? = NewsMockContent.NEWS.find { news -> news.id == newsId }

            //create notification just to show how it will work in future
            if (newsId == 6) {
                with(NotificationManagerCompat.from(this)) {
                    // notificationId is a unique int for each notification that you must define
                    notify(1, showDummyNotification().build())
                }
            }


            var image: ImageView = findViewById<ImageView>(R.id.image)
            var title: TextView = findViewById<TextView>(R.id.news_title)
            var date: TextView = findViewById<TextView>(R.id.news_date)
            var content: TextView = findViewById<TextView>(R.id.news_content)


            Glide.with(this)
                    .load(Uri.parse(news?.image))
                    .into(image)

            title.text = news?.title
            date.text = news?.date
            content.text = news?.content

            val first_related: Int = getRandom(NewsMockContent.NEWS.lastIndex, newsId)
            val recomededNews: News? = NewsMockContent.NEWS.find { news -> news.id == first_related }

            var first_related_image: ImageView = findViewById<ImageView>(R.id.first_related_image)
            var first_related_title: TextView = findViewById<TextView>(R.id.first_related_title)
            var first_related_content: TextView = findViewById<TextView>(R.id.first_related_content)

            first_related_title.text = recomededNews?.title
            first_related_content.text = recomededNews?.content!!.substring(0, 75) + "..."
            Glide.with(this)
                    .load(Uri.parse(recomededNews?.image))
                    .into(first_related_image)

            val floatingActionButton: FloatingActionButton = findViewById(R.id.news_back)
            floatingActionButton.setOnClickListener {
                goBack()
            }

            val firstRelatedCntainer: LinearLayout = findViewById(R.id.first_related_container)
            firstRelatedCntainer.setOnClickListener {
                reloadData(first_related)
            }

        }


    }

    private fun showDummyNotification(): NotificationCompat.Builder {
        val intent = Intent(this, MainActivity::class.java)
        val pemdingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val CHANNEL_ID = "my_channel"
            val name = "my_channel"
            val descriptionText = "my_channel_desc"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)

            var bm: Bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.launcher_gray)

            var mBuilder = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setBadgeIconType(R.drawable.launcher_gray)
                    .setLargeIcon(bm)
                    .setBadgeIconType(R.drawable.launcher_gray)
                    .setSmallIcon(R.drawable.launcher_gray)
                    .setContentTitle("Dodane su nove vijesti")
                    .setContentText("Pregledajte novosti iz EU fondova...")
                    /*.setStyle(NotificationCompat.BigTextStyle()
                            .bigText("Pregledajte najnovije vijesti"))*/
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                    .setLights(Color.RED, 3000, 3000)
                    .setContentIntent(pemdingIntent)
            return mBuilder
        } else {
            var mBuilder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle("INTERCEPTOR PODUZETNIČKI VODIČ")
                    .setContentText("Dodane su nove vijesti")
                    /*.setStyle(NotificationCompat.BigTextStyle()
                            .bigText("Pregledajte najnovije vijesti"))*/
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    /*.setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                    .setLights(Color.RED, 3000, 3000)*/
                    .setContentIntent(pemdingIntent)
            return mBuilder
        }


    }


    private fun reloadData(first_related: Int) {
        val intent = Intent(this, NewsActivity::class.java)
        intent.putExtra("news_id", first_related)
        this.startActivity(intent)
    }


    private fun getRandom(max: Int, not: Int): Int {
        var rand: Int = (1..max).shuffled().last()

        while (rand == not) {
            rand = (1..max).shuffled().last()
        }
        return rand
    }

    private fun goBack() {
        onBackPressed()
    }

    fun openNewTabWindow(urls: String) {
        val uris = Uri.parse(urls)
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        this.startActivity(intents)
    }
}
