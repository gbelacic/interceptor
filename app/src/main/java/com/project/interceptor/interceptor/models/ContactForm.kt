package com.project.interceptor.interceptor.models


data class ContactForm(
                   val ime: String,
                   val broj: String,
                   val email: String,
                   val poruka: String
)