package com.project.interceptor.interceptor.models

import android.net.Uri


data class Contest(val id: Int,
                   val naziv: String,
                   val vrstaPoziva: String,
                   val sektor: String,
                   val status: String,
                   val sluzbenaDokumentacija: String,
                   val datumPočetkaZaprimanjaPrijava: String,
                   val iznosBespovratnihSredstava: String,
                   val svrhaPoziva: String,
                   val predmetPoziva: String,
                   val najnižiIznosBespovratnihSredstava: String,
                   val najvišiIznosBespovratnihSredstava: String,
                   val intenzitetPotporePoziva: String,
                   val prihvatljiviPrijavitelji: String,
                   val prihvatljiviTroskovi: String,
                   val prihvatljivostProjekta: String,
                   val rokZaPrijavu: String
                   )


data class ContestGroup(val id: Int,
                        val groupName: String,
                        val contests: MutableList<Contest>,
                        val image: Uri)

