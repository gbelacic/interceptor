package com.project.interceptor.interceptor.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.project.interceptor.interceptor.R
import com.project.interceptor.interceptor.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import com.project.interceptor.interceptor.models.HomePageTab


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    //var tabbedToolbar: Toolbar = Toolbar(this)
    var adapter: ViewPagerAdapter? = null
    var viewPager: ViewPager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //set HomePage logo
     /*   Handler().postDelayed({
            *//* Create an Intent that will start the Menu-Activity. *//*
            *//*val mainIntent = Intent(this, Menu::class.java)
            startActivity(mainIntent)
            finish()*//*
            Runnable {
                run {
                    val homeIntent = Intent(this, HomeActivity::class.java)
                    startActivity(homeIntent)
                    finish()
                }
            }
        }, SPLASH_TIME_OUT)*/





        viewPager = findViewById(R.id.pager)
        adapter = ViewPagerAdapter(supportFragmentManager, this)
        viewPager!!.adapter = adapter;

        var tabLayout: TabLayout = findViewById(R.id.tabs)
        tabLayout.setupWithViewPager(viewPager)


        fab.setOnClickListener {
            openContactActivity()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.o_nama -> {
                // Handle the camera action
                val intent = Intent(this, DefaultMenuItemsTemplateActivity::class.java)
                startActivity(intent)
            }
            R.id.kontaktirajte_nas -> {
                openContactActivity()
            }
            R.id.natjecaji -> {
                openTab(HomePageTab.CONTESTS.tab)
            }
            R.id.novosti -> {
                openTab(HomePageTab.NEWS.tab)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun openContactActivity() {
        val intent = Intent(this, ContactUsActivity::class.java)
        startActivity(intent)
    }

    private fun openTab(tab: Int) {
        viewPager!!.currentItem = tab
    }

}
