package com.project.interceptor.interceptor.ui

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.project.interceptor.interceptor.R
import com.project.interceptor.interceptor.mock.ContestMockData
import com.project.interceptor.interceptor.models.Contest

class ContestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contest)

        /* set status bar to black instead od of default background color*/
        window.statusBarColor = Color.BLACK

        val bundle = intent.extras

        if (bundle != null) {
            val contestId: Int = bundle.getInt("news_id")
            Log.e("####", "" + contestId)
            val contest: Contest = ContestMockData.ALL_CONTESTS.find { contest -> contest.id == contestId }!!


            var natjecaj_naziv: TextView = findViewById<TextView>(R.id.natjecaj_naziv)
            var natjecaj_vrstaPoziva: TextView = findViewById<TextView>(R.id.natjecaj_vrstaPoziva)
            var natjecaj_sektor: TextView = findViewById<TextView>(R.id.natjecaj_sektor)
            var natjecaj_status: TextView = findViewById<TextView>(R.id.natjecaj_status)
            var natjecaj_sluzbenaDokumentacija: TextView = findViewById<TextView>(R.id.natjecaj_sluzbenaDokumentacija)
            var natjecaj_datumPočetkaZaprimanjaPrijava: TextView = findViewById<TextView>(R.id.natjecaj_datumPočetkaZaprimanjaPrijava)
            var natjecaj_iznosBespovratnihSredstava: TextView = findViewById<TextView>(R.id.natjecaj_iznosBespovratnihSredstava)
            var natjecaj_svrhaPoziva: TextView = findViewById<TextView>(R.id.natjecaj_svrhaPoziva)
            var natjecaj_predmetPoziva: TextView = findViewById<TextView>(R.id.natjecaj_predmetPoziva)
            var natjecaj_najnižiIznosBespovratnihSredstava: TextView = findViewById<TextView>(R.id.natjecaj_najnižiIznosBespovratnihSredstava)
            var natjecaj_najvišiIznosBespovratnihSredstava: TextView = findViewById<TextView>(R.id.natjecaj_najvišiIznosBespovratnihSredstava)
            var natjecaj_intenzitetPotporePoziva: TextView = findViewById<TextView>(R.id.natjecaj_intenzitetPotporePoziva)
            var natjecaj_prihvatljiviPrijavitelji: TextView = findViewById<TextView>(R.id.natjecaj_prihvatljiviPrijavitelji)
            var natjecaj_prihvatljiviTroskovi: TextView = findViewById<TextView>(R.id.natjecaj_prihvatljiviTroskovi)
            //var natjecaj_prihvatljivostProjekta: TextView = findViewById<TextView>(R.id.natjecaj_prihvatljivostProjekta)
            var natjecaj_rokZaPrijavu: TextView = findViewById<TextView>(R.id.natjecaj_rokZaPrijavu)


            /*if (contest.prihvatljivostProjekta?.isNullOrEmpty()) {
                var text = findViewById<TextView>(R.id.prihvatljivost_naslov)
                text.visibility = View.INVISIBLE
            }*/

            natjecaj_naziv.text = contest.naziv
            natjecaj_vrstaPoziva.text = contest.vrstaPoziva
            natjecaj_sektor.text = contest.sektor
            natjecaj_status.text = contest.status
            natjecaj_sluzbenaDokumentacija.text = contest.sluzbenaDokumentacija.substring(0, 28) + "..."
            natjecaj_datumPočetkaZaprimanjaPrijava.text = contest.datumPočetkaZaprimanjaPrijava
            natjecaj_iznosBespovratnihSredstava.text = contest.iznosBespovratnihSredstava
            natjecaj_svrhaPoziva.text = contest.svrhaPoziva
            natjecaj_predmetPoziva.text = contest.predmetPoziva
            natjecaj_najnižiIznosBespovratnihSredstava.text = contest.najnižiIznosBespovratnihSredstava
            natjecaj_najvišiIznosBespovratnihSredstava.text = contest.najvišiIznosBespovratnihSredstava
            natjecaj_intenzitetPotporePoziva.text = contest.intenzitetPotporePoziva
            natjecaj_prihvatljiviPrijavitelji.text = contest.prihvatljiviPrijavitelji
            natjecaj_prihvatljiviTroskovi.text = contest.prihvatljiviTroskovi
            //natjecaj_prihvatljivostProjekta.text = contest.prihvatljivostProjekta
            natjecaj_rokZaPrijavu.text = contest.rokZaPrijavu

            val openInBrowser: LinearLayout = findViewById(R.id.open_in_browser)
            openInBrowser.setOnClickListener {
                openNewTabWindow(contest.sluzbenaDokumentacija)
            }

            natjecaj_sluzbenaDokumentacija.setOnClickListener {
                openNewTabWindow(contest.sluzbenaDokumentacija)
            }
        }

        val floatingActionButton: FloatingActionButton = findViewById(R.id.contest_back)
        floatingActionButton.setOnClickListener {
            onBackPressed()
        }

        val contact_msg: LinearLayout = findViewById(R.id.contact_msg)
        contact_msg.setOnClickListener {
            goToContactPage()
        }
    }


    fun openNewTabWindow(urls: String) {
        val uris = Uri.parse(urls)
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        this.startActivity(intents)
    }

    private fun goToContactPage() {
        val intent = Intent(this, ContactUsActivity::class.java)
        startActivity(intent)
    }
}
