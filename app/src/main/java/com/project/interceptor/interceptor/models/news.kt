package com.project.interceptor.interceptor.models

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri

data class News(val id: Int, val title: String, val date: String, val content: String, val image: String)