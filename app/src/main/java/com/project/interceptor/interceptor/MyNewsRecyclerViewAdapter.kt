package com.project.interceptor.interceptor

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


import com.project.interceptor.interceptor.NewsFragment.OnListFragmentInteractionListener
import com.project.interceptor.interceptor.mock.DummyContent.DummyItem
import com.project.interceptor.interceptor.models.News

import kotlinx.android.synthetic.main.fragment_news.view.*
import java.lang.Exception
import java.security.AccessControlContext
import java.security.AccessController.getContext
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import java.io.BufferedInputStream
import java.io.InputStream
import java.net.URL
import android.os.AsyncTask.execute
import android.provider.SyncStateContract
import android.support.v4.content.ContextCompat.startActivity
import com.project.interceptor.interceptor.ui.NewsActivity
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */

class MyNewsRecyclerViewAdapter(
        private val mValues: List<News>,
        private val mListener: OnListFragmentInteractionListener?,
        private val mContext: Context)
    : RecyclerView.Adapter<MyNewsRecyclerViewAdapter.ViewHolder>() {

    //var mContext: Context? = null
    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as News

            val intent = Intent(mContext, NewsActivity::class.java)
            intent.putExtra("news_id", item.id)
            mContext.startActivity(intent)


            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.titleTextView.text = item.title
        holder.dateTextView.text = item.date
        Glide.with(mContext)
                .load(Uri.parse(item.image))
                .into(holder.imageView)

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }


    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val titleTextView: TextView = mView.item_title
        val dateTextView: TextView = mView.item_date
        val imageView: ImageView = mView.item_image

        override fun toString(): String {
            return super.toString() + " '" + titleTextView.text + "'"
        }
    }
}
