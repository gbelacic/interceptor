package com.project.interceptor.interceptor

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.ListAdapter
import android.widget.TextView
import com.bumptech.glide.Glide
import com.project.interceptor.interceptor.models.Contest
import com.project.interceptor.interceptor.models.ContestGroup

public class ExpandableListAdapter(
        private val mContext: Context,
        private val headers: MutableList<ContestGroup> )
    : BaseExpandableListAdapter() {


    override fun getGroup(groupPosition: Int): ContestGroup {
        return headers.get(groupPosition)
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hasStableIds(): Boolean {
        return true
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val convertView = layoutInflater.inflate(R.layout.contest_list_goup, null)

        val textView: TextView = convertView.findViewById(R.id.contest_group_name)
        var group: ContestGroup = getGroup(groupPosition)
        textView.text = group.groupName
        //val view: View = convertView.findViewById(R.id.contest_group_img)
        val view: ImageView = convertView.findViewById(R.id.contest_group_img)

        Glide.with(mContext)
                .load(group.image)
                .into(view)

        return convertView
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        var group: ContestGroup = getGroup(groupPosition)
        return group.contests.size
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Contest {
        var group: ContestGroup = getGroup(groupPosition)
        return group.contests.get(childPosition)
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong();
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater: LayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val convertView = layoutInflater.inflate(R.layout.contest_list_item, null)

        val textView: TextView = convertView.findViewById(R.id.contest_item_name)
        textView.text = getChild(groupPosition, childPosition).naziv

        return convertView
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getGroupCount(): Int {
        return headers.size
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



}